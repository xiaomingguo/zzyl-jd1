
package com.zzyl.vo;

import com.zzyl.base.BaseVo;
import lombok.Data;

/**
 * 用户信息DTO
 */
@Data
public class MemberVo extends BaseVo {
    /**
     * 认证id
     */
    private String authId;
    /**
     * 身份证号
     */
    private String idCardNo;
    /**
     * 身份证号是否认证 1认证
     */
    private Integer idCardNoVerify;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 姓名
     */
    private String name;

    private String avatar;

    private String openId;

    /**
     * 性别
     */
    private Integer sex;

    /**
     * 生日
     */
    private String birthday;

    /**
     * 下单次数
     */
    private Integer orderCount;

    /**
     * 是否签约
     */
    private String isSign;

    /**
     * 老人姓名
     */
    private String elderNames;

}


