package com.zzyl.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 退住老人信息响应模型
 *
 * @author itcast
 * @create 2023/12/20 16:49
 **/
@Data
@ApiModel(description = "退住老人信息响应模型")
public class RetreatElderVo {
    /**
     * 老人id
     */
    @ApiModelProperty(value = "老人id")
    private Long id;

    /**
     * 老人姓名
     */
    @ApiModelProperty(value = "老人姓名")
    private String name;

    /**
     * 老人身份证号
     */
    @ApiModelProperty(value = "老人身份证号")
    private String idCardNo;

    /**
     * 退住时间，格式：yyyy-MM-dd HH:mm:ss
     */
    @ApiModelProperty(value = "退住时间，格式：yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime checkOutTime;

    /**
     * 退住原因
     */
    @ApiModelProperty(value = "退住原因")
    private String reason;

    /**
     * 入住开始时间
     */
    @ApiModelProperty(value = "入住开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime checkInStartTime;

    /**
     * 入住结束时间
     */
    @ApiModelProperty(value = "入住结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime checkInEndTime;

    /**
     * 费用开始时间
     */
    @ApiModelProperty(value = "费用开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime costStartTime;

    /**
     * 费用结算时间
     */
    @ApiModelProperty(value = "费用结算时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime costEndTime;

    /**
     * 护理等级名称
     */
    @ApiModelProperty(value = "护理等级名称")
    private String nursingLevelName;

    /**
     * 床位号
     */
    @ApiModelProperty(value = "床位号")
    private String bedNumber;

    /**
     * 护理员名称，多个护理员以逗号隔开
     */
    @ApiModelProperty(value = "护理员名称，多个护理员以逗号隔开")
    private String nursingName;

    /**
     * 联系方式
     */
    @ApiModelProperty(value = "联系方式")
    private String phone;

    /**
     * 家庭住址
     */
    @ApiModelProperty(value = "家庭住址")
    private String address;

    /**
     * 申请人
     */
    @ApiModelProperty(value = "申请人")
    private String applicat;
}
