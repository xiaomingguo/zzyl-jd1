package com.zzyl.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 报警数据响应模型
 *
 * @author itcast
 */
@Data
@ApiModel("报警数据响应模型")
public class AlertDataVo {

    /**
     * 报警数据id
     */
    @ApiModelProperty(value = "报警数据id")
    private Long id;

    /**
     * 物联网设备id
     */
    @ApiModelProperty(value = "物联网设备id")
    private String iotId;

    /**
     * 设备名称
     */
    @ApiModelProperty(value = "设备名称")
    private String deviceName;

    /**
     * 设备备注名称
     */
    @ApiModelProperty(value = "设备备注名称")
    private String nickname;

    /**
     * 所属产品key
     */
    @ApiModelProperty(value = "所属产品key")
    private String productKey;

    /**
     * 产品名称
     */
    @ApiModelProperty(value = "产品名称")
    private String productName;

    /**
     * 功能标识符
     */
    @ApiModelProperty(value = "功能标识符")
    private String functionId;

    /**
     * 接入位置
     */
    @ApiModelProperty(value = "接入位置")
    private String accessLocation;

    /**
     * 数据值
     */
    @ApiModelProperty(value = "数据值")
    private String dataValue;

    /**
     * 报警规则id
     */
    @ApiModelProperty(value = "报警规则id")
    private String alertRuleId;

    /**
     * 报警原因，格式：功能名称+运算符号+阙值，持续x个周期就报警<br>
     * 示例：心率>=100，持续3个周期就报警;
     */
    @ApiModelProperty(value = "报警原因，格式：功能名称+运算符号+阙值，持续x个周期就报警")
    private String alertReason;

    /**
     * 处理结果
     */
    @ApiModelProperty(value = "处理结果")
    private String processingResult;

    /**
     * 处理人id
     */
    @ApiModelProperty(value = "处理人id")
    private String processorId;

    /**
     * 处理人名称
     */
    @ApiModelProperty(value = "处理人名称")
    private String processorName;

    /**
     * 处理时间
     */
    @ApiModelProperty(value = "处理时间")
    private String processingTime;

    /**
     * 报警数据类型，0：老人异常数据，1：设备异常数据
     */
    @ApiModelProperty(value = "报警数据类型，0：老人异常数据，1：设备异常数据")
    private String type;

    /**
     * 状态，0：待处理，1：已处理
     */
    @ApiModelProperty(value = "状态，0：待处理，1：已处理")
    private String status;

    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id")
    private Long userId;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
}