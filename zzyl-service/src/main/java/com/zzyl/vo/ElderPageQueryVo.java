package com.zzyl.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 老人分页查询响应模型
 *
 * @author itcast
 * @create 2023/12/19 17:20
 **/
@Data
@ApiModel(description = "老人分页查询响应模型")
public class ElderPageQueryVo {

    /**
     * 老人id
     */
    @ApiModelProperty(value = "老人id")
    private Long id;

    /**
     * 老人姓名
     */
    @ApiModelProperty(value = "老人姓名")
    private String name;

    /**
     * 老人身份证号
     */
    @ApiModelProperty(value = "老人身份证号")
    private String idCardNo;

    /**
     * 床位号
     */
    @ApiModelProperty(value = "床位号")
    private String bedNumber;
}
