
package com.zzyl.vo;

import com.zzyl.base.BaseVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 客户老人关联实体类
 */
@Data
@ApiModel(value = "MemberElderVo对象", description = "客户老人关联实体类")
public class MemberElderVo extends BaseVo {

    /**
     * 客户id
     */
    @ApiModelProperty(value = "客户id")
    private Long memberId;

    /**
     * 老人id
     */
    @ApiModelProperty(value = "老人id")
    private Long elderId;

    @ApiModelProperty(value = "老人")
    private ElderVo elderVo;

    @ApiModelProperty(value = "床位")
    private BedVo bedVo;

    @ApiModelProperty(value = "房间")
    private RoomVo roomVo;

    /**
     * 老人关联的设备
     */
    private List<DeviceVo> deviceVos;

}


