package com.zzyl.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 老人入住信息响应模型
 *
 * @author itcast
 * @create 2023/12/19 17:54
 **/
@Data
@ApiModel(description = "老人入住信息响应模型")
public class ElderCheckInInfoVo {
    /**
     * 老人id
     */
    @ApiModelProperty(value = "老人id")
    private Long id;

    /**
     * 老人姓名
     */
    @ApiModelProperty(value = "老人姓名")
    private String name;

    /**
     * 身份证号
     */
    @ApiModelProperty(value = "身份证号")
    private String idCardNo;

    /**
     * 入住开始时间
     */
    @ApiModelProperty(value = "入住开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime checkInStartTime;

    /**
     * 入住结束时间
     */
    @ApiModelProperty(value = "入住结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime checkInEndTime;

    /**
     * 费用开始时间
     */
    @ApiModelProperty(value = "费用开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime costStartTime;

    /**
     * 费用结束时间
     */
    @ApiModelProperty(value = "费用结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime costEndTime;

    /**
     * 护理等级ID
     */
    @ApiModelProperty(value = "护理等级ID")
    private Long nursingLevelId;

    /**
     * 护理等级名称
     */
    @ApiModelProperty(value = "护理等级名称")
    private String nursingLevelName;

    /**
     * 床位号
     */
    @ApiModelProperty(value = "床位号")
    private String bedNumber;

    /**
     * 护理员姓名
     */
    @ApiModelProperty(value = "护理员姓名")
    private String nursingName;

    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号")
    private String phone;

    /**
     * 家庭住址
     */
    @ApiModelProperty(value = "家庭住址")
    private String address;
}
