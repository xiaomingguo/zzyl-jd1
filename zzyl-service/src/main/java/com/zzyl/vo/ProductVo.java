package com.zzyl.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 产品信息响应模型
 *
 * @author itcast
 * @create 2023/12/4 15:29
 **/
@Data
@ApiModel("产品信息响应模型")
public class ProductVo implements Serializable {
    /**
     * 产品的ProductKey,物联网平台产品唯一标识
     */
    @ApiModelProperty("产品的ProductKey,物联网平台产品唯一标识")
    private String productKey;

    /**
     * 产品名称
     */
    @ApiModelProperty("产品名称")
    private String productName;
}
