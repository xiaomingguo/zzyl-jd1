package com.zzyl.vo;

import com.zzyl.base.BaseVo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NursingProjectVo extends BaseVo {
    private String name;
    private Integer orderNo;
    private String unit;
    private BigDecimal price;
    private String image;
    private String nursingRequirement;
    private Integer status;
    private String creator;
}
