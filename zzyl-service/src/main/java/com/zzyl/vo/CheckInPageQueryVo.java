package com.zzyl.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 入住信息分页结果响应模型
 *
 * @author itcast
 * @create 2023/12/19 14:21
 **/
@Data
@ApiModel(description = "入住信息分页结果响应模型")
public class CheckInPageQueryVo {
    /**
     * 入住id
     */
    @ApiModelProperty(value = "入住id")
    private Long id;

    /**
     * 老人姓名
     */
    @ApiModelProperty(value = "老人姓名")
    private String elderName;

    /**
     * 老人身份证号
     */
    @ApiModelProperty(value = "老人身份证号")
    private String elderIdCardNo;

    /**
     * 床位号
     */
    @ApiModelProperty(value = "床位号")
    private String bedNumber;

    /**
     * 护理等级名称
     */
    @ApiModelProperty(value = "护理等级名称")
    private String nursingLevelName;

    /**
     * 入住开始时间
     */
    @ApiModelProperty(value = "入住开始时间")
    private LocalDateTime checkInStartTime;

    /**
     * 入住结束时间
     */
    @ApiModelProperty(value = "入住结束时间")
    private LocalDateTime checkInEndTime;

    /**
     * 申请人
     */
    @ApiModelProperty(value = "申请人")
    private String applicat;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

}
