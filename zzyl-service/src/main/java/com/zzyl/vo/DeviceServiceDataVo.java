package com.zzyl.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 设备服务调用数据响应模型
 *
 * @author itcast
 * @create 2023/12/5 19:06
 **/
@Data
@ApiModel("设备服务调用数据响应模型")
public class DeviceServiceDataVo {
    /**
     * 下一页面中的服务调用记录的起始时间。<br>
     * 调用本接口查询下一页服务调用记录时，因为我们做的倒序排列，该值作为请求endTime的值。
     */
    @ApiModelProperty(value = "下一页面中的服务调用记录的起始时间", notes = "调用本接口查询下一页服务调用记录时，因为我们做的倒序排列，该值作为请求endTime的值。")
    private Long nextTime;

    /**
     * 是否有下一页服务调用记录
     */
    @ApiModelProperty("是否有下一页服务调用记录")
    private Boolean nextValid;

    /**
     * 服务调用记录集合
     */
    @ApiModelProperty("服务调用记录集合")
    private List<ServiceInfo> serviceInfoList;

    @Data
    @ApiModel("服务调用记录")
    public static class ServiceInfo {
        /**
         * 服务标识符
         */
        @ApiModelProperty("服务标识符")
        private String identifier;

        /**
         * 服务的输入参数，MAP格式的字符串，结构为key:value
         */
        @ApiModelProperty("服务的输入参数，MAP格式的字符串，结构为key:value")
        private String inputData;

        /**
         * 服务名称
         */
        @ApiModelProperty("服务名称")
        private String name;

        /**
         * 服务的输出参数，MAP格式的字符串，结构为key:value
         */
        @ApiModelProperty("服务的输出参数，MAP格式的字符串，结构为key:value")
        private String outputData;

        /**
         * 调用服务的时间
         */
        @ApiModelProperty("调用服务的时间")
        private String time;
    }
}
