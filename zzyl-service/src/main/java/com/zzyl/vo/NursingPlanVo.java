package com.zzyl.vo;

import com.zzyl.base.BaseVo;
import com.zzyl.entity.NursingProjectPlan;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NursingPlanVo extends BaseVo {
    private String planName;
    private Integer status;
    private Integer count;
    private List<NursingProjectPlanVo> projectPlans;
}
