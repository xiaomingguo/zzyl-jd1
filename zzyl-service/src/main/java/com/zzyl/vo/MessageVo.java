package com.zzyl.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 消息响应模型
 *
 * @author itcast
 * @create 2023/12/8 16:57
 **/
@Data
@ApiModel("消息响应模型")
public class MessageVo {
    /**
     * 消息id
     */
    @ApiModelProperty("消息id")
    private Long id;

    /**
     * 标题
     */
    @ApiModelProperty("标题")
    private String title;

    /**
     * 消息内容
     */
    @ApiModelProperty("消息内容")
    private String content;

    /**
     * 消息类型，0：报警通知
     */
    @ApiModelProperty("消息类型，0：报警通知")
    private Integer type;

    /**
     * 消息接收人id，特别的，后台：-1
     */
    @ApiModelProperty("消息接收人id，特别的，后台：-1")
    private Long userId;

    /**
     * 是否已读，0：未读，1：已读
     */
    @ApiModelProperty("是否已读，0：未读，1：已读")
    private Integer isRead;

    /**
     * 已读时间
     */
    @ApiModelProperty("已读时间")
    private LocalDateTime readTime;

    /**
     * 关联id,如果是报警通知，则关联报警数据id
     */
    @ApiModelProperty("关联id,如果是报警通知，则关联报警数据id")
    private Long relevantId;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;
}
