package com.zzyl.vo;

import com.zzyl.base.BaseEntity;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class DeviceDataVo extends BaseEntity {

    /**
     * 接入位置
     */
    private String accessLocation;

    /**
     * 位置类型 0：随身设备 1：固定设备
     */
    private Integer locationType;

    /**
     * 物理位置类型 0楼层 1房间 2床位
     */
    private Integer physicalLocationType;

    /**
     * 位置备注
     */
    private String deviceDescription;

    /**
     * 报警时间
     */
    private LocalDateTime alarmTime;

    /**
     * 数据值
     */
    private String dataValue;

    /**
     * 设备名称
     */
    private String deviceName;

    /**
     * 功能标识符
     */
    private String functionId;

    /**
     * 物联网设备ID
     */
    private String iotId;

    /**
     * 备注名称
     */
    private String nickname;

    /**
     * 所属产品的key
     */
    private String productKey;

    /**
     * 产品名称
     */
    private String productName;
}