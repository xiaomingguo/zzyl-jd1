package com.zzyl.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NursingPlanProjectVo {

    private Long planId;
    private Long projectId;
    private Integer executeCycle;
    private Integer executeFrequency;
    private String executeTime;
    private Integer status;
    private String remark;
    private String projectName;

}
