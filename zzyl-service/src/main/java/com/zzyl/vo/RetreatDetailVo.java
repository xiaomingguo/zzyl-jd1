package com.zzyl.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 退住详情响应模型
 *
 * @author itcast
 * @create 2023/12/20 21:41
 **/
@Data
public class RetreatDetailVo {

    /**
     * 退住老人信息响应模型
     */
    @ApiModelProperty(value = "退住老人信息响应模型")
    private RetreatElderVo retreatElderVo;

    /**
     * 解除合同时间
     */
    @ApiModelProperty(value = "解除合同时间")
    private LocalDateTime releaseDate;

    /**
     * 解除合同url
     */
    @ApiModelProperty(value = "解除合同url")
    private String releasePdfUrl;

    /**
     * 账单信息json
     */
    @ApiModelProperty(value = "账单信息json")
    private String billJson;

    /**
     * 退款凭证响应模型
     */
    @ApiModelProperty(value = "退款凭证响应模型")
    private RefundVoucherVo refundVoucherVo;
}
