package com.zzyl.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 退住分页查询响应模型
 *
 * @author itcast
 * @create 2023/12/20 21:17
 **/
@Data
@ApiModel("退住分页查询响应模型")
public class RetreatPageQueryVo {
    /**
     * 退住id
     */
    @ApiModelProperty("退住id")
    private Long id;

    /**
     * 老人姓名
     */
    @ApiModelProperty("老人姓名")
    private String elderName;

    /**
     * 老人身份证号
     */
    @ApiModelProperty("老人身份证号")
    private String elderIdCardNo;

    /**
     * 退住时间
     */
    @ApiModelProperty("退住时间")
    private LocalDateTime checkOutTime;

    /**
     * 创建人
     */
    @ApiModelProperty("创建人")
    private String applicat;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;
}
