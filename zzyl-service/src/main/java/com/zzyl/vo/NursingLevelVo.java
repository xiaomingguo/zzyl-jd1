package com.zzyl.vo;

import com.zzyl.base.BaseVo;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class NursingLevelVo extends BaseVo {

    private String name;

    private String planName;

    private Long planId;

    private BigDecimal fee;

    private Integer status;

    private String description;

    private Long cid;
}
