package com.zzyl.vo;

import com.zzyl.base.BaseVo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Alert规则Vo
 */
@Data
public class AlertRuleVo extends BaseVo {
    /**
     * 产品ID
     */
    @ApiModelProperty(value = "产品ID")
    private String productKey;

    /**
     * 产品名称
     */
    @ApiModelProperty(value = "产品名称")
    private String productName;

    /**
     * 模块ID
     */
    @ApiModelProperty(value = "模块ID")
    private String moduleId;

    /**
     * 模块名称
     */
    @ApiModelProperty(value = "模块名称")
    private String moduleName;

    /**
     * 功能名称
     */
    @ApiModelProperty(value = "功能名称")
    private String functionName;

    /**
     * 功能ID
     */
    @ApiModelProperty(value = "功能ID")
    private String functionId;

    /**
     * 物联网设备ID
     */
    @ApiModelProperty(value = "物联网设备ID")
    private String iotId;

    @ApiModelProperty(value = "设备名称")
    String deviceName;

    /**
     * 报警数据类型，0：老人异常数据，1：设备异常数据
     */
    @ApiModelProperty(value = "报警数据类型，0：老人异常数据，1：设备异常数据")
    private Integer alertDataType;

    /**
     * 规则名称
     */
    @ApiModelProperty(value = "规则名称")
    private String alertRuleName;

    /**
     * 操作符
     */
    @ApiModelProperty(value = "操作符")
    private String operator;

    /**
     * 值
     */
    @ApiModelProperty(value = "值")
    private Float value;

    /**
     * 持续时间
     */
    @ApiModelProperty(value = "持续时间")
    private Integer duration;

    /**
     * 告警生效周期
     */
    @ApiModelProperty(value = "告警生效周期")
    private String alertEffectivePeriod;

    /**
     * 告警静默期
     */
    @ApiModelProperty(value = "告警静默期")
    private Integer alertSilentPeriod;

    /**
     * 状态
     */
    @ApiModelProperty(value = "状态 0-无效 1-有效")
    private Integer status;

    private String rules;
}