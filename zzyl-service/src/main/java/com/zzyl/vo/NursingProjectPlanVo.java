package com.zzyl.vo;

import com.zzyl.base.BaseVo;
import lombok.Data;

@Data
//护理项目计划VO
public class NursingProjectPlanVo extends BaseVo {

    //计划id
    private Long planId;

    //项目id
    private Long projectId;

    //计划执行时间
    private String executeTime;

    //执行周期  0 天 1 周 2月
    private Integer executeCycle;

    //执行频次
    private Integer executeFrequency;

    //项目名称
    private String projectName;
}
