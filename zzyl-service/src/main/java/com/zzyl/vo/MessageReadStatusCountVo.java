package com.zzyl.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 消息已读未读状态统计数量响应模型
 *
 * @author itcast
 * @create 2023/12/8 16:57
 **/
@Data
@ApiModel("消息已读未读状态统计数量响应模型")
public class MessageReadStatusCountVo {
    /**
     * 未读消息数量
     */
    @ApiModelProperty("未读消息数量")
    private Long unReadCount;

    /**
     * 已读消息数量
     */
    @ApiModelProperty("已读消息数量")
    private Long completedReadCount;

    /**
     * 实例化一个消息已读未读状态统计数量响应模型
     *
     * @return 消息已读未读状态统计数量响应模型
     */
    public static MessageReadStatusCountVo instance() {
        MessageReadStatusCountVo messageReadStatusCountVo = new MessageReadStatusCountVo();
        messageReadStatusCountVo.setUnReadCount(0L);
        messageReadStatusCountVo.setCompletedReadCount(0L);
        return messageReadStatusCountVo;
    }
}
