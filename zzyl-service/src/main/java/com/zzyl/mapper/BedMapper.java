package com.zzyl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zzyl.entity.Bed;
import org.apache.ibatis.annotations.Mapper;

/**
 * 床位
 */
@Mapper
public interface BedMapper extends BaseMapper<Bed> {
}
