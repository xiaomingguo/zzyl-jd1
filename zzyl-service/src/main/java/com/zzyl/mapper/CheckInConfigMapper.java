package com.zzyl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zzyl.entity.CheckInConfig;
import org.apache.ibatis.annotations.Mapper;

/**
 * 入住配置
 */
@Mapper
public interface CheckInConfigMapper extends BaseMapper<CheckInConfig> {
}
