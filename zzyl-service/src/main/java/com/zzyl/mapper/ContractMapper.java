package com.zzyl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zzyl.entity.Contract;
import org.apache.ibatis.annotations.Mapper;

/**
 * 合同
 */
@Mapper
public interface ContractMapper extends BaseMapper<Contract> {
}
