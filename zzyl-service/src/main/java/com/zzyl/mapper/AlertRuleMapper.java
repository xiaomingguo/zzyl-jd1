package com.zzyl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zzyl.entity.AlertRule;
import org.apache.ibatis.annotations.Mapper;

/**
 * 报警规则
 */
@Mapper
public interface AlertRuleMapper extends BaseMapper<AlertRule> {
}
