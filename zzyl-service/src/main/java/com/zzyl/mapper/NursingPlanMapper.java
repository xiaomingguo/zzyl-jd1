package com.zzyl.mapper;

import com.zzyl.entity.NursingPlan;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 护理计划表 Mapper 接口
 * </p>
 *
 * @author gxm
 * @since 2024-08-15
 */
@Mapper
public interface NursingPlanMapper extends BaseMapper<NursingPlan> {

}
