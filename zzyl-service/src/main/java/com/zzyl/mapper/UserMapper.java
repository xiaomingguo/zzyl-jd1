package com.zzyl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.github.pagehelper.Page;
import com.zzyl.dto.UserDto;
import com.zzyl.entity.User;
import com.zzyl.vo.UserVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface UserMapper extends BaseMapper<User> {

    List<UserVo> findUserPage(UserDto userDto);

    @Select("select sur.user_id from sys_user_role sur left join sys_role sr on sur.role_id = sr.id where sr.role_name  = #{roleName}")
    List<Long> selectUserIdsByRoleName(String roleName);
}
