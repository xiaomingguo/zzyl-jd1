package com.zzyl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zzyl.entity.Order;
import com.zzyl.vo.OrderVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDateTime;

@Mapper
public interface OrderMapper extends BaseMapper<Order> {

    Page<OrderVo> findByPage(Page<OrderVo> page,@Param("status") Integer status, @Param("orderNo") String orderNo, @Param("elderlyName") String elderlyName, @Param("creator") String creator, @Param("startTime") LocalDateTime startTime, @Param("endTime") LocalDateTime endTime, @Param("userId") Long userId);
}
