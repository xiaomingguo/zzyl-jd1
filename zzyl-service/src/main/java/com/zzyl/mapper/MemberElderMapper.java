package com.zzyl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zzyl.entity.MemberElder;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface MemberElderMapper extends BaseMapper<MemberElder> {

    Page<MemberElder> findByPage(Page<MemberElder> page, @Param("memberId") Long memberId, @Param("elderId") Long elderId);
}
