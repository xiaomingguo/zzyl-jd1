package com.zzyl.mapper;

import com.zzyl.entity.NursingProjectPlan;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zzyl.vo.NursingProjectPlanVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 护理计划和项目关联表 Mapper 接口
 * </p>
 *
 * @author gxm
 * @since 2024-08-15
 */
@Mapper
public interface NursingProjectPlanMapper extends BaseMapper<NursingProjectPlan> {

    //根据计划id查询对应的项目详细
    @Select("select npp.*,np.name as project_name  from nursing_project_plan npp left join nursing_project np on npp.project_id = np.id where plan_id = #{planId}")
    List<NursingProjectPlanVo> findListByPlanId(Long planId);
}
