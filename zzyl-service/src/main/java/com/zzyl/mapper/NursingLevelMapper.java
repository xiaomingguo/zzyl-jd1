package com.zzyl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zzyl.entity.NursingLevel;
import com.zzyl.vo.NursingLevelVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface NursingLevelMapper extends BaseMapper<NursingLevel> {

    //分页查询
    Page<NursingLevelVo> findList(Page<NursingLevelVo> page, @Param("name")String name, @Param("status")Integer status);
}
