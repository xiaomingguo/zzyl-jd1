package com.zzyl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zzyl.entity.AlertData;
import org.apache.ibatis.annotations.Mapper;

/**
 * 报警数据
 */
@Mapper
public interface AlertDataMapper extends BaseMapper<AlertData> {
}
