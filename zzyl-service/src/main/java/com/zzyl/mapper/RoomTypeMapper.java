package com.zzyl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zzyl.entity.RoomType;
import com.zzyl.vo.RoomTypeVo;
import com.zzyl.vo.RoomVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface RoomTypeMapper extends BaseMapper<RoomType> {

    //查询房型列表
    List<RoomTypeVo> selectRoomTypeList(RoomType roomType);
}
