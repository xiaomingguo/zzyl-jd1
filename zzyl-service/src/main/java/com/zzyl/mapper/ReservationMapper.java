package com.zzyl.mapper;

import cn.hutool.core.date.DateTime;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zzyl.entity.Reservation;
import com.zzyl.vo.TimeCountVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDateTime;
import java.util.List;

@Mapper
public interface ReservationMapper extends BaseMapper<Reservation> {

    //获取指定用户在某个时间段内的取消预约次数
    @Select("select count(*) from app_reservation where update_by = #{userId} and update_time between #{begin} and #{end} and status = 2")
    int getCancelCount(DateTime begin, DateTime end, Long userId);


    //查询每个时间段剩余预约次数
    @Select("SELECT time, 6 - COUNT(*) AS count FROM app_reservation where time between #{begin} and #{end} and status != 2 group by time")
    List<TimeCountVo> getRemainingCount(LocalDateTime begin, LocalDateTime end);
}
