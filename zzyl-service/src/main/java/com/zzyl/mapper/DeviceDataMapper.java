package com.zzyl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zzyl.entity.DeviceData;
import com.zzyl.vo.DeviceDataGraphVo;
import org.apache.ibatis.annotations.Mapper;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 设备数据
 */
@Mapper
public interface DeviceDataMapper extends BaseMapper<DeviceData> {

    //按日查询设备数据
    List<DeviceDataGraphVo> queryDeviceDataListByDay(String iotId, String functionId, LocalDateTime minCreateTime, LocalDateTime maxCreateTime);

}
