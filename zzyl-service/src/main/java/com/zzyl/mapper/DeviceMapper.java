package com.zzyl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zzyl.entity.Device;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 设备
 */
@Mapper
public interface DeviceMapper extends BaseMapper<Device> {

    List<Long> selectNursingIdsByIotIdWithElder(String iotId);

    List<Long> selectNursingIdsByIotIdWithBed(String iotId);
}
