package com.zzyl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zzyl.entity.Elder;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 老人
 */
@Mapper
public interface ElderMapper extends BaseMapper<Elder> {

    @Select("select e.* from app_member_elder ame left join elder e on ame.elder_id = e.id where ame.member_id = #{memberId}")
    List<Elder> findByMemberId(Long memberId);
}
