package com.zzyl.mapper;

import com.zzyl.entity.NursingProject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 护理项目表 Mapper 接口
 * </p>
 *
 * @author gxm
 * @since 2024-08-15
 */
@Mapper
public interface NursingProjectMapper extends BaseMapper<NursingProject> {

}
