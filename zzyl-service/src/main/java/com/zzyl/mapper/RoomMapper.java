package com.zzyl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zzyl.entity.Room;
import com.zzyl.vo.RoomVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface RoomMapper extends BaseMapper<Room> {

    //查询指定楼层下的房间
    List<RoomVo> findByFloorId(Long floorId);

    //查询楼层中的房间和床位设备及数据
    List<RoomVo> getRoomsWithDeviceByFloorId(Long floorId);
}
