package com.zzyl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zzyl.entity.Floor;
import com.zzyl.vo.FloorVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 楼层
 */
@Mapper
public interface FloorMapper extends BaseMapper<Floor> {

    List<FloorVo> getAllWithRoomAndBed();

    List<Floor> getAllFloorsWithDevice();
}
