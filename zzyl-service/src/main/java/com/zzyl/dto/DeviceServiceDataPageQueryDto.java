package com.zzyl.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 设备服务调用记录分页查询请求模型
 *
 * @author itcast
 * @create 2023/12/5 18:54
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("设备服务调用记录分页查询请求模型")
public class DeviceServiceDataPageQueryDto {
    /**
     * 服务调用记录的开始时间
     */
    @ApiModelProperty(value = "服务调用记录的开始时间", required = true)
    private Long startTime;

    /**
     * 服务调用记录的结束时间
     */
    @ApiModelProperty(value = "服务调用记录的结束时间", required = true)
    private Long endTime;

    /**
     * 物联网设备id
     */
    @ApiModelProperty(value = "物联网设备id", required = true)
    private String iotId;

    /**
     * 页面大小
     */
    @ApiModelProperty(value = "页面大小", required = true)
    private Integer pageSize;
}
