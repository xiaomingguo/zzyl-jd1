package com.zzyl.dto;

import com.zzyl.base.BaseDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

/**
 * @author itheima
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("部门DTO")
public class DeptDto extends BaseDto {

    @ApiModelProperty(value = "父部门编号")
    private String parentDeptNo;

    @ApiModelProperty(value = "部门编号")
    private String deptNo;

    @ApiModelProperty(value = "部门名称")
    private String deptName;

    @ApiModelProperty(value = "排序")
    private Integer sortNo;

    @ApiModelProperty(value = "负责人Id")
    private Long leaderId;

    @ApiModelProperty(value = "是否启用（0:启用，1:禁用）")
    private String dataState;

    @ApiModelProperty(value = "层级")
    private Integer level = 4;

}
