
package com.zzyl.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * 欠款 月度欠费请求模型
 * @author itheima
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "欠款(月度欠费)请求模型")
public class ArrearageDto {

    /**
     * 账单编码
     */
    @ApiModelProperty(value = "账单编码")
    private String code;
    /**
     * 账单类型
     * 0:月度账单
     * 1:订单
     */
    @ApiModelProperty(value = "账单类型(0:月度账单,1:订单)")
    private int type;

    /**
     * 账单月份
     */
    @ApiModelProperty(value = "账单月份")
    private String billMonth;

    /**
     * 应付金额
     */
    @ApiModelProperty(value = "应付金额")
    private BigDecimal amount;
}
