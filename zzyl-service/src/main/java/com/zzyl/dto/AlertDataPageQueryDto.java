package com.zzyl.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 报警数据分页查询请求模型
 *
 * @author itcast
 * @create 2023/12/5 18:54
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("报警数据分页查询请求模型")
public class AlertDataPageQueryDto {
    /**
     * 开始报警时间
     */
    @ApiModelProperty(value = "开始报警时间", required = false)
    private Long startTime;

    /**
     * 结束报警时间
     */
    @ApiModelProperty(value = "结束报警时间", required = false)
    private Long endTime;

    /**
     * 设备名称（精确搜索）
     */
    @ApiModelProperty(value = "设备名称（精确搜索）", required = false)
    private String deviceName;

    /**
     * 设备名称
     */
    @ApiModelProperty(value = "状态，0：待处理，1：已处理", required = false)
    private Integer status;

    /**
     * 报警数据id
     */
    @ApiModelProperty(value = "报警数据id", required = false)
    private Long id;

    /**
     * 页码
     */
    @ApiModelProperty(value = "页码", example = "1", required = true)
    private Integer pageNum;

    /**
     * 页面大小
     */
    @ApiModelProperty(value = "页面大小", example = "10", required = true)
    private Integer pageSize;
}
