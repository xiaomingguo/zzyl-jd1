package com.zzyl.dto;

import com.zzyl.base.BaseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NursingElderDto extends BaseDto {
    private Long id;

    private List<Long> nursingIds;

    private Long elderId;
}