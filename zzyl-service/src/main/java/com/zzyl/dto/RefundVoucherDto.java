package com.zzyl.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author itheima
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("退款凭证上传请求模型")
public class RefundVoucherDto {

    /**
     * 退款凭证URL
     */
    @ApiModelProperty(value = "退款凭证URL")
    private String refundVoucherUrl;

    /**
     * 退款渠道【支付宝、微信、现金】
     */
    @ApiModelProperty(value = "退款渠道【支付宝、微信、现金】")
    private String tradingChannel;

    /**
     * 老人id
     */
    @ApiModelProperty(value = "老人id")
    private Long elderId;

    /**
     * 退款金额
     */
    @ApiModelProperty(value = "退款金额")
    private BigDecimal refundAmount;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;
}
