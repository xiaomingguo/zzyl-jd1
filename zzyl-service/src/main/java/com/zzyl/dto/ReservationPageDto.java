package com.zzyl.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReservationPageDto {
    private Integer pageNum;
    private Integer pageSize;
    private Integer status;
    private String name;
    private String phone;
    private Integer type;
    private Long startTime;
    private Long endTime;
    private Long userId;
}
