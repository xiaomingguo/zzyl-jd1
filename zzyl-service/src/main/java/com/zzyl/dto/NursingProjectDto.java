package com.zzyl.dto;

import com.zzyl.base.BaseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NursingProjectDto extends BaseDto {

    private String name;
    private Integer orderNo;
    private String unit;
    private BigDecimal price;
    private String image;
    private String nursingRequirement;
    private Integer status;

}
