
package com.zzyl.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zzyl.base.BaseDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @author itcast
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "合同信息")
public class CheckInContractDto extends BaseDto {
    /**
     * 合同名称
     */
    @ApiModelProperty(value = "合同名称")
    private String name;

    /**
     * 签约时间
     */
    @ApiModelProperty(value = "签约时间,格式：yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime signDate;

    /**
     * 丙方名称
     */
    @ApiModelProperty(value = "丙方名称")
    private String memberName;

    /**
     * 丙方手机号
     */
    @ApiModelProperty(value = "丙方手机号")
    private String memberPhone;

    /**
     * 合同pdf文件地址
     */
    @ApiModelProperty(value = "合同pdf文件地址")
    private String pdfUrl;
}


