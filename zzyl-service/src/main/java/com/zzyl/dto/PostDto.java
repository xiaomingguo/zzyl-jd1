package com.zzyl.dto;

import com.zzyl.base.BaseDto;
import com.zzyl.vo.DeptVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author itheima
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("岗位DTO")
public class PostDto extends BaseDto {

    @ApiModelProperty(value = "部门编号")
    private String deptNo;

    @ApiModelProperty(value = "岗位编码：父部门编号+001【3位】")
    private String postNo;

    @ApiModelProperty(value = "岗位名称")
    private String postName;

    @ApiModelProperty(value = "显示顺序")
    private Integer sortNo;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "职位对应部门")
    private DeptVo deptVo;

    @ApiModelProperty(value = "是否启用(0:启用,1:禁用)")
    private String dataState;

}
