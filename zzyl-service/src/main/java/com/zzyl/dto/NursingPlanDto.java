package com.zzyl.dto;

import com.zzyl.base.BaseDto;
import com.zzyl.entity.NursingProjectPlan;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NursingPlanDto extends BaseDto {
    private String planName;
    private Integer status;
    private List<NursingProjectPlan> projectPlans;
}
