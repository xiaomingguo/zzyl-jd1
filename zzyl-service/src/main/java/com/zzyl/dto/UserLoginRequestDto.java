package com.zzyl.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * C端用户登录
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserLoginRequestDto {

    //昵称
    private String nickName;

    //登录临时凭证
    private String code;

    //手机号临时凭证
    private String phoneCode;
}
