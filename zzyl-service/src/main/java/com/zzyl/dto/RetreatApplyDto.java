package com.zzyl.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * 申请退住请求模型
 *
 * @author itcast
 * @create 2023/12/20 16:29
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "申请退住请求模型")
public class RetreatApplyDto {

    /**
     * 退住老人信息请求模型
     */
    @ApiModelProperty(value = "退住老人信息请求模型")
    private RetreatElderDto retreatElderDto;

    /**
     * 解除合同时间
     */
    @ApiModelProperty(value = "解除合同时间")
    private LocalDateTime releaseDate;

    /**
     * 解除合同url
     */
    @ApiModelProperty(value = "解除合同url")
    private String releasePdfUrl;

    /**
     * 账单信息json
     */
    @ApiModelProperty(value = "账单信息json")
    private String billJson;

    /**
     * 退款凭证上传请求模型
     */
    @ApiModelProperty(value = "退款凭证上传请求模型")
    private RefundVoucherDto refundVoucherDto;


}
