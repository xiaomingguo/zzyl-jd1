package com.zzyl.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("设备数据分页查询请求模型")
public class DeviceDataPageReqDto {

    @ApiModelProperty(value = "设备名称", required = false)
    private String deviceName;

    @ApiModelProperty(value = "功能ID", required = false)
    private String functionId;

    @ApiModelProperty(value = "开始时间", required = false)
    private Long startTime;

    @ApiModelProperty(value = "结束时间", required = false)
    private Long endTime;

    @ApiModelProperty(value = "页码", required = true, example = "1")
    private Integer pageNum;

    @ApiModelProperty(value = "页面大小", required = true, example = "10")
    private Integer pageSize;

    @ApiModelProperty(value = "绑定位置", required = false)
    private String locationType;

    private LocalDateTime startDate;

    private LocalDateTime endDate;


}