package com.zzyl.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author itcast
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "入住配置信息")
public class CheckInConfigDto {

    @ApiModelProperty(value = "入住开始时间,格式：yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime checkInStartTime;

    @ApiModelProperty(value = "入住结束时间,格式：yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime checkInEndTime;

    @ApiModelProperty(value = "费用开始时间,格式：yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime costStartTime;

    @ApiModelProperty(value = "费用结束时间,格式：yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime costEndTime;

    @ApiModelProperty(value = "护理等级ID")
    private Long nursingLevelId;

    @ApiModelProperty(value = "护理等级名称")
    private String nursingLevelName;

    @ApiModelProperty(value = "床位Id")
    private Long bedId;

    @ApiModelProperty(value = "押金金额")
    private BigDecimal depositAmount;

    @ApiModelProperty(value = "护理费用")
    private BigDecimal nursingCost;

    @ApiModelProperty(value = "床位费用")
    private BigDecimal bedCost;

    @ApiModelProperty(value = "其他费用")
    private BigDecimal otherCost;

    @ApiModelProperty(value = "医保支付")
    private BigDecimal medicalInsurancePayment;

    @ApiModelProperty(value = "政府补贴")
    private BigDecimal governmentSubsidy;


    @ApiModelProperty("房间ID")
    private Long roomId;

    @ApiModelProperty(value = "楼层id")
    private Long floorId;

    @ApiModelProperty(value = "楼层名称")
    private String floorName;

    @ApiModelProperty(value = "房间编号")
    private String code;


}
