package com.zzyl.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerUserLoginDto {
    private String code;
    private String nickName;
    private String phoneCode;
}
