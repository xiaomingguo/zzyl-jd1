package com.zzyl.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 设备分页查询请求模型
 *
 * @author itcast
 * @create 2023/12/4 15:58
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("设备分页查询请求模型")
public class DevicePageQueryDto {
    /**
     * 设备名称
     */
    @ApiModelProperty(value = "设备名称", required = false)
    private String deviceName;

    /**
     * 产品的ProductKey,物联网平台产品唯一标识
     */
    @ApiModelProperty(value = "产品的ProductKey,物联网平台产品唯一标识", required = false)
    private String productKey;

    /**
     * 位置类型 0 随身设备 1固定设备
     */
    @ApiModelProperty(value = "位置类型 0 随身设备 1固定设备", required = false)
    private Integer locationType;

    /**
     * 页码
     */
    @ApiModelProperty(value = "页码", required = true, example = "1")
    private Integer pageNum;

    /**
     * 页面大小
     */
    @ApiModelProperty(value = "页面大小", required = true, example = "10")
    private Integer pageSize;

}
