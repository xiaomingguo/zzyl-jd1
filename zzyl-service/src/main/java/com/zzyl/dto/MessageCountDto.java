package com.zzyl.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 消息数量映射模型
 *
 * @author itcast
 * @create 2023/12/8 18:25
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageCountDto {
    /**
     * 已读未读状态，0：未读，1：已读
     */
    private Integer readStatus;

    /**
     * 数量
     */
    private Long count;
}
