package com.zzyl.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * 费用应退请求模型
 * @author itheima
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "费用应退请求模型")
public class DueBackDto {

    /**
     * 账单编码
     */
    @ApiModelProperty(value = "账单编码")
    private String code;
    /**
     * 账单类型
     * 0:月度账单
     * 1:订单
     */
    @ApiModelProperty(value = "账单类型(0:月度账单,1:订单)")
    private int type;

    /**
     * 账单月份
     */
    @ApiModelProperty(value = "账单月份")
    private String billMonth;

    /**
     * 可退金额
     */
    @ApiModelProperty(value = "可退金额")
    private BigDecimal amount;

    /**
     * 护理项目
     */
    @ApiModelProperty(value = "护理项目")
    private String nursingName;

    /**
     * 实退金额
     */
    @ApiModelProperty(value = "实退金额")
    private BigDecimal realAmount;

    /**
     * 调整备注
     */
    @ApiModelProperty(value = "调整备注")
    private String remark;


    @ApiModelProperty(value = "订单号")
    private Long tradingOrderNo;

    /**
     * 实际天数
     */
    @ApiModelProperty(value = "实际天数")
    private Integer realDay;

    /**
     * 退住天数
     */
    @ApiModelProperty(value = "退住天数")
    private Integer dueBackDay;
}
