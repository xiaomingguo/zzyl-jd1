package com.zzyl.service;

import com.aliyun.iot20180120.models.QueryDeviceDetailRequest;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zzyl.base.PageResponse;
import com.zzyl.dto.DeviceDto;
import com.zzyl.dto.DevicePageQueryDto;
import com.zzyl.entity.Device;
import com.zzyl.vo.DeviceVo;
import com.zzyl.vo.ProductVo;

import java.util.List;

/**
 * 设备
 */
public interface DeviceService extends IService<Device> {

    //从物联网平台同步产品列表
    void syncProductList();

    //查询所有产品列表
    List<ProductVo> findAllProduct();

    //新增设备
    void registerDevice(DeviceDto deviceDto);

    //分页查询设备列表
    PageResponse<Device> findByPage(DevicePageQueryDto devicePageQueryDto);

    //查询设备详细数据
    DeviceVo queryDeviceDetail(QueryDeviceDetailRequest request);
}
