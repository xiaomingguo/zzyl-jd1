package com.zzyl.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zzyl.entity.Bed;

/**
 * 床位
 */
public interface BedService extends IService<Bed> {

}
