package com.zzyl.service;

import com.zzyl.base.PageResponse;
import com.zzyl.dto.NursingPlanDto;
import com.zzyl.entity.NursingPlan;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zzyl.vo.NursingPlanVo;

/**
 * <p>
 * 护理计划表 服务类
 * </p>
 *
 * @author gxm
 * @since 2024-08-15
 */
public interface NursingPlanService extends IService<NursingPlan> {

    //分页条件查询
    PageResponse<NursingPlan> findByPage(String name, Integer status, Integer pageNum, Integer pageSize);

    //新增
    void saveNursingPlan(NursingPlanDto nursingPlanDto);

    //主键查询
    NursingPlanVo findById(Long id);

    //更新
    void updateNursingPlan(NursingPlanDto nursingPlanDto);

    //主键删除
    void deleteById(Long id);
}
