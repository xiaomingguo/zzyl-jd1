package com.zzyl.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zzyl.base.PageResponse;
import com.zzyl.entity.Order;
import com.zzyl.vo.OrderVo;

import java.time.LocalDateTime;

/**
 * 订单
 */
public interface OrderService extends IService<Order> {

    //创建订单
    void createOrder(Order order);

    //查询订单
    PageResponse<OrderVo> findByPage(Integer status, String orderNo, String elderlyName, String creator, LocalDateTime startTime, LocalDateTime endTime, Integer page, Integer pageSize);



}
