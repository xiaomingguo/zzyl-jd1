package com.zzyl.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zzyl.base.PageResponse;
import com.zzyl.entity.NursingLevel;
import com.zzyl.vo.NursingLevelVo;

/**
 * 服务等级
 */
public interface NursingLevelService extends IService<NursingLevel> {

    //分页查询
    PageResponse<NursingLevelVo> findByPage(Integer pageNum, Integer pageSize, String name, Integer status);
}
