package com.zzyl.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zzyl.base.PageResponse;
import com.zzyl.dto.MemberElderDto;
import com.zzyl.entity.MemberElder;
import com.zzyl.vo.MemberElderVo;

import java.util.List;

/**
 * 手机端用户
 */
public interface MemberElderService extends IService<MemberElder> {

    //绑定老人
    void saveMemberElder(MemberElderDto memberElderDto);

    //分页查询家人列表
    PageResponse<MemberElder> findByPage(Long memberId, Long elderId, Integer pageNum, Integer pageSize);
}
