package com.zzyl.service;

import com.zzyl.base.PageResponse;
import com.zzyl.entity.NursingProject;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 护理项目表 服务类
 * </p>
 *
 * @author gxm
 * @since 2024-08-15
 */
public interface NursingProjectService extends IService<NursingProject> {

    //分页查询
    PageResponse<NursingProject> findByPage(String name, Integer status, Integer pageNum, Integer pageSize);
}
