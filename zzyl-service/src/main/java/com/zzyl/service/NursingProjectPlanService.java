package com.zzyl.service;

import com.zzyl.entity.NursingProjectPlan;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 护理计划和项目关联表 服务类
 * </p>
 *
 * @author gxm
 * @since 2024-08-15
 */
public interface NursingProjectPlanService extends IService<NursingProjectPlan> {

}
