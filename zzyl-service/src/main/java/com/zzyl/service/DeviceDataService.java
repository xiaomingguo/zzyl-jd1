package com.zzyl.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zzyl.base.PageResponse;
import com.zzyl.dto.DeviceDataPageReqDto;
import com.zzyl.entity.DeviceData;
import com.zzyl.vo.DeviceDataGraphVo;
import com.zzyl.vo.DeviceDataVo;

import java.util.List;

/**
 * 设备数据
 */
public interface DeviceDataService extends IService<DeviceData> {
    //条件分页查询设备数据
    PageResponse<DeviceData> findByPage(DeviceDataPageReqDto deviceDataPageReqDto);

    List<DeviceDataGraphVo> queryDeviceDataListByDay(String iotId, String functionId, Long startTime, Long endTime);
}
