package com.zzyl.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zzyl.base.PageResponse;
import com.zzyl.dto.ReservationDto;
import com.zzyl.dto.ReservationPageDto;
import com.zzyl.entity.Reservation;
import com.zzyl.vo.ReservationVo;
import com.zzyl.vo.TimeCountVo;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 预约
 */
public interface ReservationService extends IService<Reservation> {

    //查询取消预约数量
    int getCancelCount();

    //查询剩余预约次数
    List<TimeCountVo> getRemainingCount(Long time);

    //新增预约
    void saveReservation(Reservation reservation);

    //分页查询我的预约
    PageResponse<Reservation> findByPage(Integer status, Integer pageNum, Integer pageSize);
}
