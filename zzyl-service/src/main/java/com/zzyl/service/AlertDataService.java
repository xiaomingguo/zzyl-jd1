package com.zzyl.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zzyl.entity.AlertData;

/**
 * 报警数据
 */
public interface AlertDataService extends IService<AlertData> {

}
