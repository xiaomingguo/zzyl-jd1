package com.zzyl.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zzyl.entity.Room;
import com.zzyl.vo.RoomVo;

import java.util.List;

/**
 * 房间
 */
public interface RoomService extends IService<Room> {

    //新增或者修改房间
    void saveOrUpdateRoom(Room room);

    //查询指定楼层下的房间
    List<RoomVo> findByFloorId(Long floorId);

    //查询房间详情
    RoomVo findRoomById(Long id);

    //查询房间及关联的床位和设备
    List<RoomVo> getRoomsWithDeviceByFloorId(Long floorId);
}
