package com.zzyl.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zzyl.base.PageResponse;
import com.zzyl.entity.NursingLevel;
import com.zzyl.mapper.NursingLevelMapper;
import com.zzyl.service.NursingLevelService;
import com.zzyl.vo.NursingLevelVo;
import org.springframework.stereotype.Service;

/**
 * 服务等级
 */
@Service
public class NursingLevelServiceImpl extends ServiceImpl<NursingLevelMapper, NursingLevel> implements NursingLevelService {

    //分页查询
    @Override
    public PageResponse<NursingLevelVo> findByPage(Integer pageNum, Integer pageSize, String name, Integer status) {
        //1. 设置分页
        Page<NursingLevelVo> page = new Page<>(pageNum, pageSize);

        //2. 执行查询
        page = getBaseMapper().findList(page, name, status);

        //3. 返回结果
        return new PageResponse<>(page);
    }
}
