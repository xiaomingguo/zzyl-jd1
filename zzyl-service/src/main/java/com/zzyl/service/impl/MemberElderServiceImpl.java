package com.zzyl.service.impl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zzyl.base.PageResponse;
import com.zzyl.entity.Elder;
import com.zzyl.exception.BusinessException;
import com.zzyl.mapper.ElderMapper;
import com.zzyl.utils.UserThreadLocal;
import com.zzyl.vo.ElderVo;
import com.google.common.collect.Lists;
import com.zzyl.vo.RoomVo;
import java.util.Map;
import java.time.LocalDateTime;
import com.zzyl.vo.BedVo;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zzyl.dto.MemberElderDto;
import com.zzyl.entity.MemberElder;
import com.zzyl.mapper.MemberElderMapper;
import com.zzyl.service.MemberElderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MemberElderServiceImpl extends ServiceImpl<MemberElderMapper, MemberElder> implements MemberElderService {


    @Autowired
    private MemberElderMapper memberElderMapper;

    @Autowired
    private ElderMapper elderMapper;

    @Override
    public void saveMemberElder(MemberElderDto memberElderDto) {
        //根据身份证号和姓名查询老人信息
        LambdaQueryWrapper<Elder> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Elder::getIdCardNo,memberElderDto.getIdCard())
                .eq(Elder::getName,memberElderDto.getName())
                .between(Elder::getStatus,1,4);//状态
        Elder elder = elderMapper.selectOne(wrapper);
        if (elder == null){
            throw new BusinessException("绑定老人失败");
        }

        //新增数据
        MemberElder memberElder = new MemberElder();
        memberElder.setMemberId(UserThreadLocal.get());
        memberElder.setElderId(elder.getId());
        memberElder.setRemark(memberElderDto.getRemark());
        memberElderMapper.insert(memberElder);
    }

    @Override
    public PageResponse<MemberElder> findByPage(Long memberId, Long elderId, Integer pageNum, Integer pageSize) {
        Page<MemberElder> page = new Page<>(pageNum,pageSize);

        page = getBaseMapper().findByPage(page,memberId, elderId);

        return new PageResponse<>(page);
    }
}
