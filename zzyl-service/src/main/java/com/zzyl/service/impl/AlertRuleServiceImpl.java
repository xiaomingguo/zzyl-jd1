package com.zzyl.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zzyl.base.PageResponse;
import com.zzyl.entity.AlertData;
import com.zzyl.entity.AlertRule;
import com.zzyl.mapper.AlertRuleMapper;
import com.zzyl.mapper.DeviceMapper;
import com.zzyl.mapper.UserMapper;
import com.zzyl.service.AlertDataService;
import com.zzyl.service.AlertRuleService;
import com.zzyl.vo.AlertRuleVo;
import com.zzyl.vo.DeviceDataVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * 报警规则
 */
@Service
public class AlertRuleServiceImpl extends ServiceImpl<AlertRuleMapper, AlertRule> implements AlertRuleService {

    @Override
    public PageResponse<AlertRuleVo> getAlertRulePage(Integer pageNum, Integer pageSize, String alertRuleName, String productKey, String functionName) {

        Page<AlertRule> page = new Page(pageNum, pageSize);

        LambdaQueryWrapper<AlertRule> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(StrUtil.isNotEmpty(alertRuleName), AlertRule::getAlertRuleName, alertRuleName)
                .eq(StrUtil.isNotEmpty(productKey), AlertRule::getProductKey, productKey)
                .eq(StrUtil.isNotEmpty(functionName), AlertRule::getFunctionName, functionName);
        page = getBaseMapper().selectPage(page, wrapper);

        List<AlertRuleVo> list = page.getRecords().stream().map(v -> {
            AlertRuleVo alertRuleVo = BeanUtil.copyProperties(v, AlertRuleVo.class);
            alertRuleVo.setRules(new StringBuilder(v.getFunctionName()).append(v.getOperator()).append(v.getValue()).append("持续触发").append(v.getDuration()).append("个周期时发生报警").toString());
            return alertRuleVo;
        }).collect(Collectors.toList());

        return new PageResponse(page, list);
    }

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private DeviceMapper deviceMapper;

    @Autowired
    private AlertDataService alertsDataService;

    @Autowired
    private UserMapper userMapper;

    //设备维护人员的角色名称
    @Value("${zzyl.alert.deviceMaintainerRole}")
    private String deviceMaintainerRole;
    //超级管理员的角色名称
    @Value("${zzyl.alert.managerRole}")
    private String managerRole;


    /**
     * 校验设备上报数据，进行报警规则过滤处理
     *
     * @param deviceDataVo 设备上报数据
     */
    @Override
    public void alertFilter(DeviceDataVo deviceDataVo) {
        //1. 获取设备上报数据时间，如果上报发生在1分钟前(1分钟前的数据应该已经被前一次定时任务处理过)，不再处理
        LocalDateTime alarmTime = deviceDataVo.getAlarmTime();
        long between = LocalDateTimeUtil.between(alarmTime, LocalDateTime.now(), ChronoUnit.SECONDS);
        if (between > 60) {
            return;
        }

        //2. 查询应用在当前设备(有专门针对于当前设备的,还有全部设备的)当前功能上的所有规则
        LambdaQueryWrapper<AlertRule> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(AlertRule::getFunctionId, deviceDataVo.getFunctionId())
                .eq(AlertRule::getProductKey, deviceDataVo.getProductKey())
                .in(AlertRule::getIotId, deviceDataVo.getIotId(), "-1");
        List<AlertRule> alertRules = getBaseMapper().selectList(wrapper);
        //如果报警规则为空，程序结束
        if (CollectionUtil.isEmpty(alertRules)) {
            return;
        }

        //如果报警规则不为空，遍历报警规则，进行校验
        alertRules.forEach(alertRule -> deviceDateAlarmHandler(deviceDataVo, alertRule));
    }

    /**
     * 设备数据报警处理器
     *
     * @param deviceDataVo 一台设备上报过来的一条数据
     * @param alertRule    一条报警规则
     */
    private void deviceDateAlarmHandler(DeviceDataVo deviceDataVo, AlertRule alertRule) {
        //第一层校验：判断是否在生效时段内
        String[] aepArr = alertRule.getAlertEffectivePeriod().split("~");
        LocalTime startTime = LocalTime.parse(aepArr[0]);
        LocalTime endTime = LocalTime.parse(aepArr[1]);
        LocalTime time = LocalDateTimeUtil.of(deviceDataVo.getAlarmTime()).toLocalTime();//上报时间
        //如果上报时间不在生效时间段内，校验结束
        if (startTime.isAfter(time) || endTime.isBefore(time)) {
            return;
        }

        //设备id
        String iotId = deviceDataVo.getIotId();
        //从redis中获取数据，报警规则连续触发次数rediskey
        String aggCountKey = "alert_trigger_count:" + iotId + ":" + deviceDataVo.getFunctionId() + ":" + alertRule.getId();

        //第二层校验：将设备上报值和阈值比较
        //x – 第一个值，y – 第二个值。x==y返回0，x<y返回小于0的数，x>y返回大于0的数
        int compareResult = NumberUtil.compare(Double.parseDouble(deviceDataVo.getDataValue()), alertRule.getValue());
        //或 逻辑运算，2个条件满足其一即为true
        //1.运算符为>=，且设备上报值 >= 阈值
        //2.运算符为<，且设备上报值 < 阈值
        if ((ObjectUtil.equals(alertRule.getOperator(), ">=") && compareResult >= 0)
                || (ObjectUtil.equals(alertRule.getOperator(), "<") && compareResult < 0)) {
            log.debug("此时设备上报数据达到阈值");
        } else {
            //该情况不符合报警规则，所以设备上报数据为正常数据。需要删除redis聚合的异常数据,程序结束
            redisTemplate.delete(aggCountKey);
            return;
        }

        //第三层校验：校验当前设备+功能的报警规则是否处于沉默周期
        String silentCacheKey = "alert_silent:" + iotId + ":" + deviceDataVo.getFunctionId() + ":" + alertRule.getId();
        String silentData = redisTemplate.opsForValue().get(silentCacheKey);
        //如果redis中存在对应沉默周期数据，则当前设备+功能正处于沉默周期，无需报警
        if (StrUtil.isNotEmpty(silentData)) {
            return;
        }

        //第四层校验：校验持续周期
        //4.1获取连续触发报警规则的次数
        //aggCountData是上次触发报警累计到的次数，这一次又触发了报警，所以累积到这次就+1
        String aggCountData = redisTemplate.opsForValue().get(aggCountKey);
        int triggerCount = ObjectUtil.isEmpty(aggCountData) ? 1 : Integer.parseInt(aggCountData) + 1;

        //4.2判断次数是否与持续周期相等
        //如果触发报警规则次数不等于持续周期，则无需报警
        if (ObjectUtil.notEqual(alertRule.getDuration(), triggerCount)) {
            redisTemplate.opsForValue().set(aggCountKey, triggerCount + "");
            return;
        }

        //如果触发报警规则次数等于持续周期，则需要报警，完成以下操作：
        //1）删除报警规则连续触发次数的缓存
        //2）添加对应的沉默周期，设置过期时间添加对应的沉默周期，设置过期时间
        //3）报警数据存储到数据库
        redisTemplate.delete(aggCountKey);
        redisTemplate.opsForValue().set(silentCacheKey, deviceDataVo.getDataValue(), alertRule.getAlertSilentPeriod(), TimeUnit.MINUTES);

        //获取消息的消费者
        List<Long> consumerIds = null;
        if (ObjectUtil.equals(0, alertRule.getAlertDataType())) {
            //如果是老人报警数据，需要通知绑定老人的护理员。根据iotId查询老人绑定的护理员
            if (deviceDataVo.getLocationType() == 0) {
                consumerIds = deviceMapper.selectNursingIdsByIotIdWithElder(iotId);
            } else if (deviceDataVo.getLocationType() == 1 && deviceDataVo.getPhysicalLocationType() == 2) {
                consumerIds = deviceMapper.selectNursingIdsByIotIdWithBed(iotId);
            }
        } else {
            //如果是设备报警数据，需要通知设备维护人员。根据指定角色名称查询相关用户
            consumerIds = userMapper.selectUserIdsByRoleName(deviceMaintainerRole);
        }
        //查询超级管理员，超级管理员无论什么消息都会接收
        List<Long> managerIds = userMapper.selectUserIdsByRoleName(managerRole);
        List<Long> allConsumerIds = CollUtil.addAllIfNotContains(consumerIds, managerIds);
        allConsumerIds = CollUtil.distinct(allConsumerIds);

        //对接消息队列，将消息发送到消息队列中

        //新增报警数据
        insertAlertData(deviceDataVo, alertRule, allConsumerIds);
    }

    //新增报警数据
    private void insertAlertData(DeviceDataVo deviceDataVo, AlertRule alertRule, List<Long> consumerIds) {
        String alertReason = CharSequenceUtil.format("{}{}{},持续{}个周期就报警", alertRule.getFunctionName(), alertRule.getOperator(), alertRule.getValue(), alertRule.getDuration());
        AlertData alertData = BeanUtil.toBean(deviceDataVo, AlertData.class);
        alertData.setAlertRuleId(alertRule.getId());
        alertData.setAlertReason(alertReason);
        alertData.setType(alertRule.getAlertDataType());
        alertData.setStatus(0);

        List<AlertData> list = consumerIds.stream().map(id -> {
            AlertData dbAlertData = BeanUtil.toBean(alertData, AlertData.class);
            dbAlertData.setUserId(id);
            return dbAlertData;
        }).collect(Collectors.toList());
        alertsDataService.saveBatch(list);
    }
}
