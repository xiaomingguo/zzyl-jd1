package com.zzyl.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zzyl.entity.*;
import com.zzyl.exception.BusinessException;
import com.zzyl.mapper.*;
import com.zzyl.service.RoomService;
import com.zzyl.vo.BedVo;
import com.zzyl.vo.DeviceDataVo;
import com.zzyl.vo.DeviceVo;
import com.zzyl.vo.RoomVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RoomServiceImpl extends ServiceImpl<RoomMapper, Room> implements RoomService {

    @Autowired
    private RoomTypeMapper roomTypeMapper;

    @Autowired
    private BedMapper bedMapper;

    @Autowired
    private CheckInConfigMapper checkInConfigMapper;

    @Autowired
    private ElderMapper elderMapper;

    @Override
    public void saveOrUpdateRoom(Room room) {
        //1. 根据typeName去房型表中查询到房型的id
        LambdaQueryWrapper<RoomType> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(RoomType::getName, room.getTypeName());
        RoomType roomType = roomTypeMapper.selectOne(wrapper);
        if (roomType == null) {
            throw new BusinessException("您选择的房型有误");
        }
        room.setTypeId(roomType.getId());

        //2. 保存或者更新
        if (room.getId() == null){
            getBaseMapper().insert(room);
        }else{
            getBaseMapper().updateById(room);
        }
    }

    @Override
    public List<RoomVo> findByFloorId(Long floorId) {
        return getBaseMapper().findByFloorId(floorId);
    }

    @Override
    public RoomVo findRoomById(Long id) {
        //1. 直接查询房间表  房间id 房间code 房间sort 房间typeId 房间typeName
        Room room = getBaseMapper().selectById(id);
        //使用工具类 将room对象中属性复制到vo对象
        RoomVo roomVo = BeanUtil.copyProperties(room, RoomVo.class);

        //2. 根据room_id从base_bed表查询当前房间的床位信息
        //查询所有(list)  主键查询(getById)  条件查询(封装器)
        LambdaQueryWrapper<Bed> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Bed::getRoomId, id);
        List<Bed> bedList = bedMapper.selectList(wrapper);

        //3. 总床位数  入住床位数  入住率
        int totalBeds = bedList.size();
        int occupiedBeds = (int) (bedList.stream().filter(e -> e.getBedStatus() == 1).count());
        if (totalBeds != 0 && occupiedBeds != 0) {
            roomVo.setOccupancyRate(occupiedBeds * 1.0 / totalBeds);
        } else {
            roomVo.setOccupancyRate(0.0);
        }
        roomVo.setTotalBeds(totalBeds);
        roomVo.setOccupiedBeds(occupiedBeds);

        //4. 搞定床位集合
        List<BedVo> bedVoList = bedList.stream().map(e -> {
            //4-1 创建VO
            BedVo bedVo = new BedVo();

            //4-2 床位编号
            bedVo.setBedNumber(e.getBedNumber());

            //4-3 服务相关
            //根据床位号从入住配置表中查询 服务等级
            LambdaQueryWrapper<CheckInConfig> wrapper1 = new LambdaQueryWrapper<>();
            wrapper1.eq(CheckInConfig::getBedNumber, e.getBedNumber());
            CheckInConfig checkInConfig = checkInConfigMapper.selectOne(wrapper1);
            if (checkInConfig != null) {
                bedVo.setLname(checkInConfig.getNursingLevelName());//服务等级名称
                bedVo.getCheckInConfigVo().setCheckInStartTime(checkInConfig.getCheckInStartTime());
                bedVo.getCheckInConfigVo().setCheckInEndTime(checkInConfig.getCheckInEndTime());
            }

            //4-4 根据床位id查询老人信息
            LambdaQueryWrapper<Elder> wrapper2 = new LambdaQueryWrapper<>();
            wrapper2.eq(Elder::getBedId, e.getId());
            Elder elder = elderMapper.selectOne(wrapper2);
            if (elder != null) {
                bedVo.setName(elder.getName());
            }

            //4-5 返回vo
            return bedVo;
        }).collect(Collectors.toList());
        roomVo.setBedVoList(bedVoList);

        return roomVo;
    }

    @Autowired
    private RedisTemplate redisTemplate;
    @Override
    public List<RoomVo> getRoomsWithDeviceByFloorId(Long floorId) {
        List<RoomVo> roomVoList = getBaseMapper().getRoomsWithDeviceByFloorId(floorId);

        for (RoomVo roomVo : roomVoList) {
            //1. 补齐房间中的设备数据信息
            for (DeviceVo deviceVo : roomVo.getDeviceVos()) {
                //deviceVo: 某个房间中的一个设备
                String jsonStr = (String)redisTemplate.opsForHash().get("DEVICE_LAST_DATA",deviceVo.getIotId());
                //将json字符串转换为List<DeviceData>
                List<DeviceDataVo> list = JSONUtil.toList(jsonStr, DeviceDataVo.class);
                //赋值
                deviceVo.setDeviceDataVos(list);
            }
            roomVo.setStatus(2);//告诉前端 有数据


            //2. 补齐床位中设备信息
            for (BedVo bedVo : roomVo.getBedVoList()) {
                for (DeviceVo deviceVo : bedVo.getDeviceVos()) {
                    //deviceVo: 某个床位中的一个设备
                    String jsonStr = (String)redisTemplate.opsForHash().get("DEVICE_LAST_DATA",deviceVo.getIotId());
                    //将json字符串转换为List<DeviceData>
                    List<DeviceDataVo> list = JSONUtil.toList(jsonStr, DeviceDataVo.class);
                    //赋值
                    deviceVo.setDeviceDataVos(list);
                }
                //设置一个状态,告诉前端设备中有数据
                bedVo.setStatus(2);
            }
        }
        return roomVoList;
    }
}
