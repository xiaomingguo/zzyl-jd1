package com.zzyl.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zzyl.base.PageResponse;
import com.zzyl.dto.NursingPlanDto;
import com.zzyl.entity.NursingPlan;
import com.zzyl.entity.NursingProject;
import com.zzyl.entity.NursingProjectPlan;
import com.zzyl.entity.User;
import com.zzyl.mapper.NursingPlanMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zzyl.mapper.NursingProjectPlanMapper;
import com.zzyl.mapper.UserMapper;
import com.zzyl.service.NursingPlanService;
import com.zzyl.vo.NursingPlanVo;
import com.zzyl.vo.NursingProjectPlanVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 护理计划表 服务实现类
 * </p>
 *
 * @author gxm
 * @since 2024-08-15
 */
@Service
public class NursingPlanServiceImpl extends ServiceImpl<NursingPlanMapper, NursingPlan> implements NursingPlanService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private NursingProjectPlanMapper nursingProjectPlanMapper;

    @Override
    public PageResponse<NursingPlan> findByPage(String name, Integer status, Integer pageNum, Integer pageSize) {
        //1. 分页查询查询护理项目表 条件name status
        //1-1 设置分页条件
        Page<NursingPlan> page = new Page<>(pageNum, pageSize);
        //1-2 设置业务条件
        LambdaQueryWrapper<NursingPlan> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(StrUtil.isNotEmpty(name), NursingPlan::getPlanName, name)
                .eq(status != null, NursingPlan::getStatus, status);
        //1-3 执行查询
        page = getBaseMapper().selectPage(page, wrapper);

        //2. 遍历page对象中的records
        page.getRecords().stream().forEach(e -> {
            User user = userMapper.selectById(e.getCreateBy());
            if (user != null) {
                e.setCreator(user.getRealName());
            }
        });

        System.out.println(page);

        //3 返回结果
        return new PageResponse<>(page);
    }

    //一个方法中向数据库发送了多条增删改语句, 注意事务问题
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveNursingPlan(NursingPlanDto nursingPlanDto) {
        //1. 从dto中获取planName封装到NursingPlan对象中, 保存到计划表
        NursingPlan nursingPlan = BeanUtil.copyProperties(nursingPlanDto, NursingPlan.class);
        //设置状态
        nursingPlan.setStatus(1);//启用
        //mp技术在新增之后会自动执行主键返回
        getBaseMapper().insert(nursingPlan);

        //2. 从dto中获取跟项目相关的计划信息, 循环保存到中间表
        nursingPlanDto.getProjectPlans().stream().forEach(nursingProjectPlan -> {
            //设置计划id
            nursingProjectPlan.setPlanId(nursingPlan.getId());
            //执行保存
            nursingProjectPlanMapper.insert(nursingProjectPlan);
        });
    }

    @Override
    public NursingPlanVo findById(Long id) {
        //1. 根据计划id查询计划表
        NursingPlan nursingPlan = getBaseMapper().selectById(id);
        NursingPlanVo nursingPlanVo = BeanUtil.copyProperties(nursingPlan, NursingPlanVo.class);

        //2. 根据计划id从中间表中查询
        List<NursingProjectPlanVo> list =  nursingProjectPlanMapper.findListByPlanId(id);
        nursingPlanVo.setProjectPlans(list);

        return nursingPlanVo;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateNursingPlan(NursingPlanDto nursingPlanDto) {
        //1. 根据id更新计划表
        NursingPlan nursingPlan = BeanUtil.copyProperties(nursingPlanDto, NursingPlan.class);
        getBaseMapper().updateById(nursingPlan);

        //2. 先根据计划id删除中间表数据
        //delete from npp where plan_id = 计划id
        LambdaQueryWrapper<NursingProjectPlan> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(NursingProjectPlan::getPlanId,nursingPlanDto.getId());
        nursingProjectPlanMapper.delete(wrapper);

        //3. 再重新添加
        nursingPlanDto.getProjectPlans().stream().forEach(nursingProjectPlan -> {
            //设置计划id
            nursingProjectPlan.setPlanId(nursingPlan.getId());
            //执行保存
            nursingProjectPlanMapper.insert(nursingProjectPlan);
        });
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deleteById(Long id) {
        //1. 根据plan_id删除中间表中数据
        LambdaQueryWrapper<NursingProjectPlan> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(NursingProjectPlan::getPlanId,id);
        nursingProjectPlanMapper.delete(wrapper);

        //2. 根据计划id删除计划表
        getBaseMapper().deleteById(id);
    }

}
