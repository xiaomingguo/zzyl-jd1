package com.zzyl.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zzyl.entity.Floor;
import com.zzyl.mapper.FloorMapper;
import com.zzyl.service.FloorService;
import com.zzyl.vo.FloorVo;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 楼层
 */
@Service
public class FloorServiceImpl extends ServiceImpl<FloorMapper, Floor> implements FloorService {

    @Override
    public List<FloorVo> getAllWithRoomAndBed() {
        return getBaseMapper().getAllWithRoomAndBed();
    }

    @Override
    public List<Floor> getAllFloorsWithDevice() {
        return getBaseMapper().getAllFloorsWithDevice();
    }
}
