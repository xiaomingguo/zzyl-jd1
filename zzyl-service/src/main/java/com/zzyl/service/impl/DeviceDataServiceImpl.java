package com.zzyl.service.impl;

import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zzyl.base.PageResponse;
import com.zzyl.dto.DeviceDataPageReqDto;
import com.zzyl.entity.DeviceData;
import com.zzyl.mapper.DeviceDataMapper;
import com.zzyl.service.DeviceDataService;
import com.zzyl.vo.DeviceDataGraphVo;
import com.zzyl.vo.DeviceDataVo;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 设备数据
 */
@Service
public class DeviceDataServiceImpl extends ServiceImpl<DeviceDataMapper, DeviceData> implements DeviceDataService {
    @Override
    public PageResponse<DeviceData> findByPage(DeviceDataPageReqDto dto) {
        //1. 分页参数
        Page<DeviceData> page = new Page<>(dto.getPageNum(), dto.getPageSize());

        //2. 业务参数
        LambdaQueryWrapper<DeviceData> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(StrUtil.isNotEmpty(dto.getDeviceName()), DeviceData::getDeviceName, dto.getDeviceName())
                .eq(StrUtil.isNotEmpty(dto.getFunctionId()), DeviceData::getFunctionId, dto.getFunctionId())
                .between(dto.getStartDate() != null && dto.getEndDate() != null, DeviceData::getAlarmTime, dto.getStartDate(), dto.getEndDate());

        //3. 执行查询
        page = getBaseMapper().selectPage(page, wrapper);
        return new PageResponse(page);
    }

    @Override
    public List<DeviceDataGraphVo> queryDeviceDataListByDay(String iotId, String functionId, Long startTime, Long endTime) {
        LocalDateTime start = LocalDateTimeUtil.of(startTime);
        LocalDateTime end = LocalDateTimeUtil.of(endTime);

        //1. 调用mapper查询现有数据
        List<DeviceDataGraphVo> list = getBaseMapper().queryDeviceDataListByDay(iotId, functionId, start, end);
        //使用Lambda的方式将上面list转换为map
        Map<String, Double> map = list.stream().collect(Collectors.toMap(DeviceDataGraphVo::getDateTime, DeviceDataGraphVo::getDataValue));

        //2. 构建一个全天集合
        List<DeviceDataGraphVo> allList = DeviceDataGraphVo.dayInstance(start);

        //3. 遍历集合  填充数据
        for (DeviceDataGraphVo deviceDataGraphVo : allList) {
            Double value = map.get(deviceDataGraphVo.getDateTime());
            deviceDataGraphVo.setDataValue(value == null ? 0.0 : value);
        }


        return allList;
    }
}
