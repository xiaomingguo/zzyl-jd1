package com.zzyl.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.aliyun.iot20180120.Client;
import com.aliyun.iot20180120.models.*;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zzyl.base.PageResponse;
import com.zzyl.dto.DeviceDto;
import com.zzyl.dto.DevicePageQueryDto;
import com.zzyl.entity.Device;
import com.zzyl.exception.BusinessException;
import com.zzyl.mapper.DeviceMapper;
import com.zzyl.properties.AliIoTConfigProperties;
import com.zzyl.service.DeviceService;
import com.zzyl.vo.DeviceVo;
import com.zzyl.vo.ProductVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 设备
 */
@Service
@Slf4j
public class DeviceServiceImpl extends ServiceImpl<DeviceMapper, Device> implements DeviceService {

    @Autowired
    private Client client;

    @Autowired
    private AliIoTConfigProperties aliIoTConfigProperties;

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public void syncProductList() {
        log.info("============开始同步云数据==================");

        //1. 从阿里云平台获取到产品信息
        //for: 知道次数  while: 不知道次数
        int page = 1;
        while (true) {
            //封装查询参数
            QueryProductListRequest request = new QueryProductListRequest();
            request.setCurrentPage(page++);//第几页
            request.setPageSize(1);//每页条数
            request.setIotInstanceId(aliIoTConfigProperties.getIotInstanceId());//实例id

            QueryProductListResponse response = null;
            try {
                response = client.queryProductList(request);
            } catch (Exception e) {
                throw new BusinessException("调用阿里云失败");
            }

            //当查询的结果中没有数据的时候,代表查到了最后一页了 退出死循环
            if (CollUtil.isEmpty(response.getBody().getData().getList().getProductInfo())) {
                log.info("============结束同步云数据==================");
                break; //return
            }

            //获取查询到的内容 对象--ProductVo
            List<ProductVo> voList = response.getBody().getData().getList().getProductInfo().stream().map(e -> {
                return BeanUtil.copyProperties(e, ProductVo.class);
            }).collect(Collectors.toList());

            if (page == 2) {//某一次点击了开始同步后的第一页
                redisTemplate.delete("PRODUCT_LIST");//删除
            }

            //2. 将产品信息保存到redis
            redisTemplate.opsForList().leftPushAll("PRODUCT_LIST", voList);
        }
    }

    @Override
    public List<ProductVo> findAllProduct() {
        //从Redis中获取PRODUCT_LIST键对应的所有值
        //LIST range 0 -1
        return redisTemplate.opsForList().range("PRODUCT_LIST", 0, -1);
    }

    @Override
    public void registerDevice(DeviceDto deviceDto) {
        //1. 根据设备名称查询数据库, 是否已经有, 如果有就报错
        LambdaQueryWrapper<Device> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Device::getDeviceName, deviceDto.getDeviceName());
        Long count = getBaseMapper().selectCount(wrapper);
        if (count > 0) {
            throw new BusinessException("设备名称已存在,请修改");
        }

        //2. 调用iot接口,将设备保存到阿里云
        RegisterDeviceRequest request = deviceDto.getRegisterDeviceRequest();//deviceName,nickName,productKey
        request.setIotInstanceId(aliIoTConfigProperties.getIotInstanceId());//InstanceId
        RegisterDeviceResponse registerDeviceResponse = null;
        try {
            registerDeviceResponse = client.registerDevice(request);
        } catch (Exception e) {
            throw new BusinessException("向IOT注册失败,请重试");
        }

        //registerDeviceResponse

        //3. 准备向数据库保存需要的参数
        Device device = BeanUtil.copyProperties(deviceDto, Device.class);
        //iotId
        device.setIotId(registerDeviceResponse.getBody().getData().getIotId());
        //productName
        ProductVo productVo = findAllProduct().stream()
                .filter(e -> StrUtil.equals(e.getProductKey(), deviceDto.getProductKey()))
                .collect(Collectors.toList())
                .stream().findFirst().get();
        device.setProductName(productVo.getProductName());
        //随身设备:  physical_location_type=-1
        if (deviceDto.getLocationType() == 0) {
            device.setPhysicalLocationType(-1);
        }

        //4. 保存数据到monitor_device表, 如果报错失败,逆向删除iot中刚刚添加好的设备
        try {
            getBaseMapper().insert(device);
        } catch (Exception e) {
            //逆向删除iot中刚刚添加好的设备
            try {
                DeleteDeviceRequest deleteRequest = new DeleteDeviceRequest();
                deleteRequest.setDeviceName(device.getDeviceName());
                deleteRequest.setIotId(device.getIotId());
                deleteRequest.setProductKey(device.getProductKey());
                deleteRequest.setIotInstanceId(aliIoTConfigProperties.getIotInstanceId());
                client.deleteDevice(deleteRequest);
            } catch (Exception exception) {
                throw new BusinessException("向IOT注册失败,请重试");
            }
        }
    }

    @Override
    public PageResponse<Device> findByPage(DevicePageQueryDto devicePageQueryDto) {
        //1. 设置分页条件
        Page<Device> page = new Page<>(devicePageQueryDto.getPageNum(), devicePageQueryDto.getPageSize());

        //2. 设置查询条件
        LambdaQueryWrapper<Device> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(StrUtil.isNotEmpty(devicePageQueryDto.getProductKey()), Device::getProductKey, devicePageQueryDto.getProductKey())
                .eq(devicePageQueryDto.getLocationType() != null, Device::getLocationType, devicePageQueryDto.getLocationType())
                .like(StrUtil.isNotEmpty(devicePageQueryDto.getDeviceName()), Device::getDeviceName, devicePageQueryDto.getDeviceName())
                .orderByDesc(Device::getCreateTime);

        //3. 执行查询
        page = getBaseMapper().selectPage(page, wrapper);

        //4. 查询创建人
        page.getRecords().stream().forEach(e -> e.setCreator("超级管理员"));

        return new PageResponse<>(page);
    }

    @Override
    public DeviceVo queryDeviceDetail(QueryDeviceDetailRequest request) {
        //1. 阿里云IOT中查询当前设备的信息
        request.setIotInstanceId(aliIoTConfigProperties.getIotInstanceId());
        QueryDeviceDetailResponse queryDeviceDetailResponse = null;
        try {
            queryDeviceDetailResponse = client.queryDeviceDetail(request);
        } catch (Exception e) {
            throw new BusinessException("从阿里云获取设备信息失败");
        }

        //2. 去数据库中查询当前设备信息
        LambdaQueryWrapper<Device> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Device::getIotId,request.getIotId());
        Device device = getBaseMapper().selectOne(wrapper);

        //3. 组装返回
        DeviceVo deviceVo = BeanUtil.copyProperties(device, DeviceVo.class);
        BeanUtil.copyProperties(queryDeviceDetailResponse.getBody().getData(),deviceVo, CopyOptions.create().ignoreNullValue());

        return deviceVo;
    }
}
