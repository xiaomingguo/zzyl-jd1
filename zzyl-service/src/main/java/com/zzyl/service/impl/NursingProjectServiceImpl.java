package com.zzyl.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zzyl.base.PageResponse;
import com.zzyl.entity.NursingProject;
import com.zzyl.entity.User;
import com.zzyl.mapper.NursingProjectMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zzyl.mapper.UserMapper;
import com.zzyl.service.NursingProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 护理项目表 服务实现类
 * </p>
 *
 * @author gxm
 * @since 2024-08-15
 */
@Service
public class NursingProjectServiceImpl extends ServiceImpl<NursingProjectMapper, NursingProject> implements NursingProjectService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public PageResponse<NursingProject> findByPage(String name, Integer status, Integer pageNum, Integer pageSize) {
        //1. 分页查询查询护理项目表 条件name status
        //1-1 设置分页条件
        Page<NursingProject> page = new Page<>(pageNum, pageSize);
        //1-2 设置业务条件
        LambdaQueryWrapper<NursingProject> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(StrUtil.isNotEmpty(name), NursingProject::getName, name)
                .eq(status != null, NursingProject::getStatus, status);
        //1-3 执行查询
        page = getBaseMapper().selectPage(page,wrapper);

        //2. 遍历page对象中的records
        page.getRecords().stream().forEach(e->{
            User user = userMapper.selectById(e.getCreateBy());
            if (user != null){
                e.setCreator(user.getRealName());
            }
        });

        System.out.println(page);

        //3 返回结果
        return new PageResponse<>(page);
    }
}
