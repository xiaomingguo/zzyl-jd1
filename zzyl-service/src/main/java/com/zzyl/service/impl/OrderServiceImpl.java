package com.zzyl.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zzyl.base.PageResponse;
import com.zzyl.entity.Order;
import com.zzyl.mapper.OrderMapper;
import com.zzyl.service.OrderService;
import com.zzyl.utils.UUID;
import com.zzyl.utils.UserThreadLocal;
import com.zzyl.vo.OrderVo;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * 订单
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {

    //创建订单
    @Override
    public void createOrder(Order order) {
        //1. 保存订单信息
        order.setStatus(0);//订单状态: 待支付
        order.setPaymentStatus(1);//支付状态: 待支付
        order.setOrderNo(UUID.randomUUID().toString());//订单号
        order.setMemberId(UserThreadLocal.getUserId());//会员号
        getBaseMapper().insert(order);//保存订单
    }

    public PageResponse<OrderVo> findByPage(Integer status, String orderNo, String elderlyName, String creator, LocalDateTime startTime, LocalDateTime endTime, Integer pageNum, Integer pageSize) {

        Page<OrderVo> page = new Page<>(pageNum, pageSize);

        Long userId = UserThreadLocal.getUserId();
        page = getBaseMapper().findByPage(page, status, orderNo, elderlyName, creator, startTime, endTime, userId);


        return new PageResponse(page);
    }
}
