package com.zzyl.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zzyl.base.PageResponse;
import com.zzyl.entity.Elder;
import com.zzyl.mapper.ElderMapper;
import com.zzyl.service.ElderService;
import com.zzyl.vo.ElderVo;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 老人
 */
@Service
public class ElderServiceImpl extends ServiceImpl<ElderMapper, Elder> implements ElderService {

    @Override
    public PageResponse<Elder> findByPage(String name, String idCardNo, String status, Integer pageNum, Integer pageSize) {
        Page<Elder> page = new Page<>(pageNum, pageSize);

        LambdaQueryWrapper<Elder> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(StrUtil.isNotEmpty(name), Elder::getName, name);
        wrapper.eq(StrUtil.isNotEmpty(idCardNo), Elder::getIdCardNo, idCardNo);
        wrapper.eq(StrUtil.isNotEmpty(status), Elder::getStatus, status);

        getBaseMapper().selectPage(page,wrapper);
        return new PageResponse<>(page);
    }
}
