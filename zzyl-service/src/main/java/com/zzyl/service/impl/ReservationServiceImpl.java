package com.zzyl.service.impl;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.LocalDateTimeUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zzyl.base.PageResponse;
import com.zzyl.entity.Member;
import com.zzyl.entity.Reservation;
import com.zzyl.mapper.MemberMapper;
import com.zzyl.mapper.ReservationMapper;
import com.zzyl.service.ReservationService;
import com.zzyl.utils.UserThreadLocal;
import com.zzyl.vo.TimeCountVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Service
public class ReservationServiceImpl extends ServiceImpl<ReservationMapper, Reservation> implements ReservationService {

    @Autowired
    private ReservationMapper reservationMapper;

    @Override
    public int getCancelCount() {
        //当前用户id
        Long userId = UserThreadLocal.get();
        //获取今天开始时间
        DateTime begin = DateUtil.beginOfDay(new Date());
        //获取今天结束时间
        DateTime end = DateUtil.endOfDay(new Date());

        return reservationMapper.getCancelCount(begin,end,userId);
    }

    @Override
    public List<TimeCountVo> getRemainingCount(Long time) {
        LocalDateTime begin = LocalDateTimeUtil.of(time);//将Long类型数字转换为时间
        LocalDateTime end = begin.plusHours(24);
        return reservationMapper.getRemainingCount(begin,end);
    }

    @Override
    public void saveReservation(Reservation reservation) {
        // 判断一下当前用户的取消次数
        int count = getCancelCount();
        if (count > 3){
            throw new RuntimeException("您已预约3次，请明天再试");
        }

        //设置状态
        reservation.setStatus(0); //待报到

        //新增
        try {
            reservationMapper.insert(reservation);
        }catch (Exception e){
            throw new RuntimeException("同一个手机号, 同一时间段只能预约一次");
        }
    }

    @Autowired
    private MemberMapper memberMapper;

    @Override
    public PageResponse<Reservation> findByPage(Integer status, Integer pageNum, Integer pageSize) {
        //分页查询我的预约
        Page<Reservation> page = new Page<>(pageNum,pageSize);

        LambdaQueryWrapper<Reservation> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Reservation::getCreateBy,UserThreadLocal.get())
                .eq(status != null,Reservation::getStatus,status);

        page = getBaseMapper().selectPage(page, wrapper);

        //补齐创建人
        page.getRecords().stream().forEach(reservation -> {
            Member member = memberMapper.selectById(reservation.getCreateBy());
            if (member != null){
                reservation.setCreator(member.getName());
            }
        });

        return new PageResponse<>(page);
    }
}
