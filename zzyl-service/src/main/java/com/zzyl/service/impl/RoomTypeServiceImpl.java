package com.zzyl.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zzyl.entity.Room;
import com.zzyl.entity.RoomType;
import com.zzyl.mapper.RoomMapper;
import com.zzyl.mapper.RoomTypeMapper;
import com.zzyl.service.RoomTypeService;
import com.zzyl.vo.RoomTypeVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 房型
 */
@Service
public class RoomTypeServiceImpl extends ServiceImpl<RoomTypeMapper, RoomType> implements RoomTypeService {

    @Autowired
    private RoomMapper roomMapper;

    //查找所有房间类型
    @Override
    public List<RoomTypeVo> findList(RoomType roomType) {
        List<RoomTypeVo> roomTypeList = getBaseMapper().selectRoomTypeList(roomType);

        //统计每种房型的房间数量
        roomTypeList.forEach(e -> {
            LambdaQueryWrapper<Room> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(Room::getTypeName, e.getName());
            Long count = roomMapper.selectCount(wrapper);
            e.setRoomCount(count.intValue());
        });

        return roomTypeList;
    }
}
