package com.zzyl.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zzyl.entity.AlertData;
import com.zzyl.mapper.AlertDataMapper;
import com.zzyl.service.AlertDataService;
import org.springframework.stereotype.Service;

/**
 * 报警数据
 */
@Service
public class AlertDataServiceImpl extends ServiceImpl<AlertDataMapper, AlertData> implements AlertDataService {

}
