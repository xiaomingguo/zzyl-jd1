package com.zzyl.service.impl;

import com.zzyl.entity.NursingProjectPlan;
import com.zzyl.mapper.NursingProjectPlanMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zzyl.service.NursingProjectPlanService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 护理计划和项目关联表 服务实现类
 * </p>
 *
 * @author gxm
 * @since 2024-08-15
 */
@Service
public class NursingProjectPlanServiceImpl extends ServiceImpl<NursingProjectPlanMapper, NursingProjectPlan> implements NursingProjectPlanService {

}
