package com.zzyl.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zzyl.base.PageResponse;
import com.zzyl.dto.CustomerUserLoginDto;
import com.zzyl.entity.Member;
import com.zzyl.vo.LoginVo;
import com.zzyl.vo.MemberVo;

/**
 * 手机端用户
 */
public interface MemberService extends IService<Member> {

    //微信登录
    LoginVo login(CustomerUserLoginDto dto);

    //分页查询
    PageResponse<MemberVo> findByPage(String nickname, String phone, Integer pageNum, Integer pageSize);
}
