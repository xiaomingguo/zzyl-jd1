package com.zzyl.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zzyl.base.PageResponse;
import com.zzyl.entity.Elder;
import com.zzyl.vo.ElderVo;

import java.util.List;

/**
 * 老人
 */
public interface ElderService extends IService<Elder> {

    //老人分页查询
    PageResponse<Elder> findByPage(String name, String idCardNo, String status, Integer pageNum, Integer pageSize);
}
