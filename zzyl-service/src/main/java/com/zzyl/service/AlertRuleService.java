package com.zzyl.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zzyl.base.PageResponse;
import com.zzyl.entity.AlertRule;
import com.zzyl.vo.AlertRuleVo;
import com.zzyl.vo.DeviceDataVo;

/**
 * 报警规则
 */
public interface AlertRuleService extends IService<AlertRule> {

    PageResponse<AlertRuleVo> getAlertRulePage(Integer pageNum, Integer pageSize, String alertRuleName, String productKey, String functionName);

    //校验设备上报数据，进行报警规则过滤处理
    void alertFilter(DeviceDataVo deviceDataVo);
}
