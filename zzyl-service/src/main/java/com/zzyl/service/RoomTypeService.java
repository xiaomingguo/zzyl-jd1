package com.zzyl.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zzyl.entity.RoomType;
import com.zzyl.vo.RoomTypeVo;

import java.util.List;

/**
 * 房型
 */
public interface RoomTypeService extends IService<RoomType> {

    //查找所有房间类型
    List<RoomTypeVo> findList(RoomType roomType);

}
