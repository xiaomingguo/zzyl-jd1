package com.zzyl.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zzyl.entity.Floor;
import com.zzyl.vo.FloorVo;

import java.util.List;

/**
 * 楼层
 */
public interface FloorService extends IService<Floor> {

    //查询所有房间和床位
    List<FloorVo> getAllWithRoomAndBed();

    //查询包含智能设备的楼层
    List<Floor> getAllFloorsWithDevice();
}
