package com.zzyl.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zzyl.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 护理项目-计划关系
 */
@TableName("nursing_project_plan")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class NursingProjectPlan extends BaseEntity {

    /**
     * 计划id
     */
    private Long planId;

    /**
     * 项目id
     */
    private Long projectId;

    /**
     * 计划执行时间
     */
    private String executeTime;

    /**
     * 执行周期
     */
    private Integer executeCycle;

    /**
     * 执行频次
     */
    private Integer executeFrequency;
}
