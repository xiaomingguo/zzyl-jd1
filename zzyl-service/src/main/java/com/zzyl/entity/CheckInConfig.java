package com.zzyl.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;

import lombok.*;
import lombok.experimental.Accessors;

/**
 * 入住陪住
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("pro_check_in_config")
public class CheckInConfig implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 老人ID
     */
    @TableField("elder_id")
    private Long elderId;

    /**
     * 入住编码
     */
    @TableField("check_in_code")
    private String checkInCode;

    /**
     * 入住开始时间
     */
    @TableField("check_in_start_time")
    private LocalDateTime checkInStartTime;

    /**
     * 入住结束时间
     */
    @TableField("check_in_end_time")
    private LocalDateTime checkInEndTime;

    /**
     * 护理等级ID
     */
    @TableField("nursing_level_id")
    private Long nursingLevelId;

    /**
     * 护理等级名称
     */
    @TableField("nursing_level_name")
    private String nursingLevelName;

    /**
     * 床位号
     */
    @TableField("bed_number")
    private String bedNumber;

    /**
     * 费用开始时间
     */
    @TableField("cost_start_time")
    private LocalDateTime costStartTime;

    /**
     * 费用结束时间
     */
    @TableField("cost_end_time")
    private LocalDateTime costEndTime;

    /**
     * 押金（元）
     */
    @TableField("deposit_amount")
    private BigDecimal depositAmount;

    /**
     * 护理费用（元/月）
     */
    @TableField("nursing_cost")
    private BigDecimal nursingCost;

    /**
     * 床位费用（元/月）
     */
    @TableField("bed_cost")
    private BigDecimal bedCost;

    /**
     * 其他费用（元/月）
     */
    @TableField("other_cost")
    private BigDecimal otherCost;

    /**
     * 医保支付（元/月）
     */
    @TableField("medical_insurance_payment")
    private BigDecimal medicalInsurancePayment;

    /**
     * 政府补贴（元/月）
     */
    @TableField("government_subsidy")
    private BigDecimal governmentSubsidy;
}
