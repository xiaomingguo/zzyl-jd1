package com.zzyl.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zzyl.base.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * 房型
 */
@TableName("base_room_type")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoomType extends BaseEntity {

    /**
     * 房型名称
     */
    private String name;

    /**
     * 床位数量
     */
    private Integer bedCount;

    /**
     * 床位费用
     */
    private BigDecimal price;

    /**
     * 介绍
     */
    private String introduction;

    /**
     * 照片
     */
    private String photo;


    /**
     * 状态，0：禁用，1：启用
     */
    private Integer status;
}
