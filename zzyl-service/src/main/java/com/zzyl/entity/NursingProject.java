package com.zzyl.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zzyl.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * 护理项目
 */
@TableName("nursing_project")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class NursingProject extends BaseEntity {

    private String name;
    private Integer orderNo;
    private String unit;
    private BigDecimal price;
    private String image;
    private String nursingRequirement;
    private Integer status;

    //不加这个 前端会出现编辑 删除按钮无法点击的情况
    @TableField(exist = false)
    private Integer count = 0;
}
