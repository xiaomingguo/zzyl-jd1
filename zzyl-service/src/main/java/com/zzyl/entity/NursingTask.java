package com.zzyl.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 护理任务表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("nursing_task")
public class NursingTask implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 护理员id
     */
    private Long nursingId;

    /**
     * 项目id
     */
    private Integer projectId;

    /**
     * 老人id
     */
    private Long elderId;

    /**
     * 床位编号
     */
    private String bedNumber;

    /**
     * 任务类型（2：月度任务，1订单任务）
     */
    private Integer taskType;

    /**
     * 预计服务时间
     */
    private LocalDateTime estimatedServerTime;

    /**
     * 实际服务时间
     */
    private LocalDateTime realServerTime;

    /**
     * 执行记录
     */
    private String mark;

    /**
     * 取消原因
     */
    private String cancelReason;

    /**
     * 状态  1待执行 2已执行 3已关闭
     */
    private Integer status;

    /**
     * 关联单据编号
     */
    private String relNo;

    /**
     * 执行图片
     */
    private String taskImage;
}
