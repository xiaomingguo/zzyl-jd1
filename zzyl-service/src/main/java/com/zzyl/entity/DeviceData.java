package com.zzyl.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zzyl.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * 设备数据
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("monitor_device_data")
public class DeviceData extends BaseEntity {

    /**
     * 接入位置
     */
    private String accessLocation;

    /**
     * 位置类型 0：随身设备 1：固定设备
     */
    private Integer locationType;

    /**
     * 物理位置类型 0楼层 1房间 2床位
     */
    private Integer physicalLocationType;

    /**
     * 位置备注
     */
    private String deviceDescription;

    /**
     * 报警时间
     */
    private LocalDateTime alarmTime;

    /**
     * 数据值
     */
    private String dataValue;

    /**
     * 设备名称
     */
    private String deviceName;

    /**
     * 功能标识符
     */
    private String functionId;

    /**
     * 物联网设备ID
     */
    private String iotId;

    /**
     * 备注名称
     */
    private String nickname;

    /**
     * 所属产品的key
     */
    private String productKey;

    /**
     * 产品名称
     */
    private String productName;
}