package com.zzyl.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zzyl.base.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 老人
 */
@TableName("elder")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Elder extends BaseEntity {

    /**
     * 姓名
     */
    private String name;

    /**
     * 头像
     */
    private String image;

    /**
     * 状态（0：禁用，1:启用  2:请假 3:退住中 4入住中 5已退住）
     */
    private Integer status;

    /**
     * 身份证号
     */
    private String idCardNo;

    /**
     * 手机号
     */
    private String phone;

    @ApiModelProperty(value = "年龄")
    @TableField(exist = false)
    private String age;

    @ApiModelProperty(value = "性别")
    private String sex;

    /**
     * 床位编号
     */
    private String bedNumber;

    /**
     * 床位ID
     */
    private Long bedId;


}

