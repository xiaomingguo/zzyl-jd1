package com.zzyl.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zzyl.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 护理-老人对应
 */
@TableName("nursing_elder")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class NursingElder extends BaseEntity {

    /**
     *  护理员ID
     */
    private Long nursingId;

    /**
     * 老人ID
     */
    private Long elderId;
}