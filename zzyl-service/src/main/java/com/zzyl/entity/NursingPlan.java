package com.zzyl.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zzyl.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 护理计划
 */
@TableName("nursing_plan")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class NursingPlan extends BaseEntity {
    private String planName;
    private Integer status;
}
