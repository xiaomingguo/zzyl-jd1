package com.zzyl.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zzyl.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.ibatis.annotations.Mapper;

import java.time.LocalDateTime;

/**
 * 预约
 */
@TableName("app_reservation")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Reservation extends BaseEntity {


    private String name;

    private String mobile;

    private LocalDateTime time;

    private String visitor;

    private Integer type;

    private Integer status;

}
