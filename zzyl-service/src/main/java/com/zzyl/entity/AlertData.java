package com.zzyl.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zzyl.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * 报警数据
 */
@TableName("monitor_alert_data")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AlertData extends BaseEntity {

    private static final long serialVersionUID = -4958513062287152699L;

    /**
     * 物联网设备id
     */
    private String iotId;

    /**
     * 设备名称
     */
    private String deviceName;

    /**
     * 设备备注名称
     */
    private String nickname;

    /**
     * 所属产品key
     */
    private String productKey;

    /**
     * 产品名称
     */
    private String productName;

    /**
     * 功能标识符
     */
    private String functionId;

    /**
     * 接入位置
     */
    private String accessLocation;

    /**
     * 位置类型 0：随身设备 1：固定设备
     */
    private Integer locationType;

    /**
     * 物理位置类型 0楼层 1房间 2床位
     */
    private Integer physicalLocationType;

    /**
     * 位置备注
     */
    private String deviceDescription;

    /**
     * 数据值
     */
    private String dataValue;

    /**
     * 报警规则id
     */
    private Long alertRuleId;

    /**
     * 报警原因，格式：功能名称+运算符+阈值+持续周期+聚合周期
     */
    private String alertReason;

    /**
     * 处理结果
     */
    private String processingResult;

    /**
     * 处理人id
     */
    private Long processorId;

    /**
     * 处理人名称
     */
    private String processorName;

    /**
     * 处理时间
     */
    private LocalDateTime processingTime;

    /**
     * 报警数据类型，0：老人异常数据，1：设备异常数据
     */
    private Integer type;

    /**
     * 状态，0：待处理，1：已处理
     */
    private Integer status;

    /**
     * 接收者id
     */
    private Long userId;
}