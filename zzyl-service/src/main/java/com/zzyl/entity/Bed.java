package com.zzyl.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zzyl.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

//床位
@TableName("base_bed")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Bed extends BaseEntity {

    /**
     * 床位编号
     */
    private String bedNumber;
    /**
     * 床位状态
     */
    private Integer bedStatus;
    /**
     * 房间ID
     */
    private Long roomId;

    /**
     * 排序号
     */
    private Integer sort;
}
