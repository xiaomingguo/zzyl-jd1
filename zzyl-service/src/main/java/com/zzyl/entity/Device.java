package com.zzyl.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zzyl.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 设备
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("monitor_device")
public class Device extends BaseEntity {

    /**
     * 物联网设备ID
     */
    private String iotId;

    /**
     * 绑定位置
     */
    private String bindingLocation;

    /**
     * 位置类型 0：随身设备 1：固定设备
     */
    private Integer locationType;

    /**
     * 物理位置类型 0楼层 1房间 2床位
     */
    private Integer physicalLocationType;

    /**
     * 设备名称
     */
    private String deviceName;

    /**
     * 位置备注
     */
    private String deviceDescription;

    /**
     * 产品是否包含门禁，0：否，1：是
     */
    private Integer haveEntranceGuard;

    /**
     * 备注名称
     */
    private String nickname;

    /**
     * 产品key
     */
    private String productKey;

    /**
     *产品名称
     */
    public String productName;

}