package com.zzyl.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zzyl.base.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 房间
 */
@TableName("base_room")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Room extends BaseEntity {

    /**
     * 房间编号
     */
    private String code;

    /**
     * 排序号
     */
    private Integer sort;

    /**
     * 房型id
     */
    private Long typeId;

    /**
     * 房间类型名称
     */
    private String typeName;

    /**
     * 楼层id
     */
    private Long floorId;

}
