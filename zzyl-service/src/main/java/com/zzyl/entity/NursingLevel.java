package com.zzyl.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zzyl.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * 护理等级
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("nursing_level")
public class NursingLevel extends BaseEntity {

    /**
     * 等级名称
     */
    private String name;

    /**
     * 护理计划ID
     */
    private Long planId;

    /**
     * 护理费用
     */
    private BigDecimal fee;

    /**
     * 状态（0：禁用，1：启用）
     */
    private Integer status;

    /**
     * 等级说明
     */
    private String description;
}
