package com.zzyl.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zzyl.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 角色-资源
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_role_resource")
public class RoleResource extends BaseEntity {

    /**
     * 数据状态（0正常 1停用）
     */
    private String dataState;

    /**
     * 资源编号

     */
    private String resourceNo;

    /**
     * 角色ID
     */
    private Long roleId;
}
