package com.zzyl.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zzyl.base.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 楼层
 */
@TableName("base_floor")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Floor extends BaseEntity {

    /**
     * 楼层名称
     */
    private String name;

    /**
     * 楼层编号
     */
    private Integer code;
}

