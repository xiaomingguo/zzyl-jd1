package com.zzyl.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zzyl.base.BaseEntity;
import com.zzyl.vo.BedVo;
import com.zzyl.vo.DeviceVo;
import com.zzyl.vo.ElderVo;
import com.zzyl.vo.RoomVo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 客户老人关联实体类
 */
@TableName("app_member_elder")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MemberElder extends BaseEntity {

    /**
     * 客户id
     */
    private Long memberId;

    /**
     * 老人id
     */
    private Long elderId;

    @TableField(exist = false)
    private ElderVo elderVo;

    @TableField(exist = false)
    private BedVo bedVo;

    @TableField(exist = false)
    private RoomVo roomVo;

    @TableField(exist = false)
    private Map deviceDataVos = new HashMap(16);

    /**
     * 老人关联的设备
     */
    @TableField(exist = false)
    private List<DeviceVo> deviceVos;
}

