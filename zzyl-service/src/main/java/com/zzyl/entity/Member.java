package com.zzyl.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zzyl.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * C端 用户
 */
@TableName("app_member")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Member extends BaseEntity {
    private String phone;
    private String name;
    private String avatar;
    private String openId;
    private Integer gender;
}
