package com.zzyl.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.zzyl.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * 登记
 */
@TableName("app_visit")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Visit extends BaseEntity {

    /**
     * 来访人
     */
    private String name;

    /**
     * 来访人手机号
     */
    private String mobile;

    /**
     * 来访时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime time;

    /**
     * 探访人
     */
    private String visitor;

    /**
     * 来访类型，0：参观来访，1：探访来访
     */
    private Integer type;

    /**
     * 来访状态，0：待报道，1：已完成，2：取消，3：过期
     */
    private Integer status;
}
