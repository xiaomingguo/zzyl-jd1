package com.zzyl.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zzyl.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 报警规则
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("monitor_alert_rule")
public class AlertRule extends BaseEntity {

    /**
     * 报警生效时段
     */
    private String alertEffectivePeriod;

    /**
     * 报警数据类型，0：老人异常数据，1：设备异常数据
     */
    private Integer alertDataType;

    /**
     * 告警规则名称
     */
    private String alertRuleName;

    /**
     * 报警沉默周期
     */
    private Integer alertSilentPeriod;

    /**
     * 设备名称
     */
    private String deviceName;

    /**
     * 持续周期
     */
    private Integer duration;

    /**
     * 功能标识
     */
    private String functionId;

    /**
     * 功能名称
     */
    private String functionName;

    /**
     * 模块的key
     */
    private String moduleId;

    /**
     * 模块名称
     */
    private String moduleName;

    /**
     * 运算符
     */
    private String operator;

    /**
     * 所属产品的key
     */
    private String productKey;

    /**
     * 产品名称
     */
    private String productName;

    /**
     * 物联网设备id
     */
    private String iotId;

    /**
     * 备注
     */
    private String remark;

    /**
     * 0 禁用 1启用
     */
    private Integer status;

    /**
     * 阈值
     */
    private Float value;


}