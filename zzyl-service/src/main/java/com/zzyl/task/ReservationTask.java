package com.zzyl.task;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.zzyl.entity.Reservation;
import com.zzyl.mapper.ReservationMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@Slf4j
public class ReservationTask {

    @Autowired
    private ReservationMapper reservationMapper;

    //定时取消预约
    @Scheduled(cron = "0 12,40 * * * ?")
    public void cancelReservation() {
        log.info("开始清理过期预约===================");
        //将过期的预约设置为status=3
        //update app_reservation set status = 3 where time < now() and status = 0
        //1. 设置set部分
        Reservation reservation = new Reservation();
        reservation.setStatus(3);//过期

        //2. 设置where方法
        LambdaUpdateWrapper<Reservation> wrapper = new LambdaUpdateWrapper<>();
        wrapper.lt(Reservation::getTime, LocalDateTime.now())
                .eq(Reservation::getStatus, 0);

        //3. 执行更新
        reservationMapper.update(reservation,wrapper);
        log.info("清理过期预约结束===================");
    }
}
