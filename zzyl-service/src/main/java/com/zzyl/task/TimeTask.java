package com.zzyl.task;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

//自动打印时间类
//@Component
public class TimeTask {

    //方法: 在指定的事件去做什么事
    //@Scheduled(fixedRate=5000)//间隔5秒执行一次
    @Scheduled(cron="0/5 * * * * *")//间隔5秒执行一次
    public void printTime(){
        System.out.println("当前时间:"+System.currentTimeMillis());
    }
}
