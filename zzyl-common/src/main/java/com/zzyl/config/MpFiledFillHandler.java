package com.zzyl.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.zzyl.utils.UserThreadLocal;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

//mp自动填充
@Slf4j
@Component
public class MpFiledFillHandler implements MetaObjectHandler {

    //新增时执行此方法
    @Override
    public void insertFill(MetaObject metaObject) {
        this.strictInsertFill(metaObject, "createTime", LocalDateTime.class, LocalDateTime.now());
        this.strictInsertFill(metaObject, "updateTime",  LocalDateTime.class, LocalDateTime.now());

        //小程序端填充用户信息
        Long userId = UserThreadLocal.get();
        if (userId != null){
            this.strictInsertFill(metaObject, "createBy", Long.class, userId);
            this.strictInsertFill(metaObject, "updateBy",  Long.class, userId);
        }
    }

    //修改时执行此方法
    @Override
    public void updateFill(MetaObject metaObject) {
        this.strictUpdateFill(metaObject, "updateTime",  LocalDateTime.class, LocalDateTime.now());

        //小程序端填充用户信息
        Long userId = UserThreadLocal.get();
        if (userId != null){
            this.strictInsertFill(metaObject, "updateBy",  Long.class, userId);
        }
    }
}