package com.zzyl.base;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 实体基础类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseEntity implements Serializable {
    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    public Long id;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)//新增时自动填充
    public LocalDateTime createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)//新增和修改时自动填充
    public LocalDateTime updateTime;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人Id")
    @TableField(fill = FieldFill.INSERT)//新增时自动填充
    private Long createBy;

    /**
     * 更新人
     */
    @ApiModelProperty(value = "更新人Id")
    @TableField(fill = FieldFill.INSERT_UPDATE)//新增和修改时自动填充
    private Long updateBy;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人名称")
    @TableField(exist = false)
    private String creator;

    /**
     * 更新人
     */
    @ApiModelProperty(value = "更新人名称")
    @TableField(exist = false)
    private String updater;
}
