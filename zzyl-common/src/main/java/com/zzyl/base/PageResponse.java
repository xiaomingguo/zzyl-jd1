package com.zzyl.base;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.Collections;
import java.util.List;

/**
 * 分页结果包装
 */
@ApiModel(value = "分页数据消息体", description = "分页数据统一对象")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageResponse<T> {

    @ApiModelProperty(value = "总条目数", required = true)
    private Long total = 0L;

    @ApiModelProperty(value = "页尺寸", required = true)
    private Long pageSize = 0L;

    @ApiModelProperty(value = "总页数", required = true)
    private Long pages = 0L;

    @ApiModelProperty(value = "页码", required = true)
    private Long page = 0L;

    @ApiModelProperty(value = "数据列表", required = true)
    private List<T> records = Collections.EMPTY_LIST;

    /**
     * 返回分页对象
     *
     * @param page mp的page对象
     */
    public PageResponse(Page page) {
        this.total = page.getTotal();
        this.pageSize = page.getSize();
        this.pages = page.getPages();
        this.page = page.getCurrent();
        this.records = page.getRecords();
    }

    /**
     * 返回分页对象
     *
     * @param page mp的page对象
     * @param list 当前页数据集合
     */
    public PageResponse(Page page, List<T> list) {
        this.total = page.getTotal();
        this.pageSize = page.getSize();
        this.pages = page.getPages();
        this.page = page.getCurrent();
        this.records = list;
    }
}
