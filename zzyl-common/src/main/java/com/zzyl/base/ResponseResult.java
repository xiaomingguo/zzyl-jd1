package com.zzyl.base;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 返回结果
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseResult implements Serializable {

    /**
     * 响应返回编码
     */
    private int code;

    /**
     * 响应返回信息
     */
    private String msg;

    /**
     * 返回结果
     */
    private Object data;


    /**
     * 返回成功消息
     *
     * @return 成功消息
     */
    public static ResponseResult success() {
        return new ResponseResult (200,"操作成功", null);
    }

    /**
     * 返回成功数据
     *
     * @return 成功消息
     */
    public static ResponseResult success(Object data) {
        return new ResponseResult (200,"操作成功", data);
    }

    /**
     * 返回错误消息
     *
     * @param msg 返回内容
     * @return 警告消息
     */
    public static ResponseResult error(String msg) {
        return new ResponseResult (500,msg, null);
    }

}
