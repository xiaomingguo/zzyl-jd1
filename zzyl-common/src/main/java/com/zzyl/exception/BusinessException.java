package com.zzyl.exception;

/**
 * 自定义业务异常
 *
 * @author itheima
 **/
public class BusinessException extends RuntimeException {

    public BusinessException(String message) {
        super(message);
    }
}
