package com.zzyl.exception;

import com.zzyl.base.ResponseResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

//全局异常处理器
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    /**
     * 处理自定义异常
     */
    @ExceptionHandler(BusinessException.class)
    public ResponseResult handleBusinessException(BusinessException exception) {
        exception.printStackTrace();

        return ResponseResult.error(exception.getMessage());
    }

    /**
     * 处理数据库 重复键的异常报错
     */
    @ExceptionHandler(DuplicateKeyException.class)
    public ResponseResult handlerDuplicateKeyException(DuplicateKeyException e) {
        e.printStackTrace();

        return ResponseResult.error("字段重复");
    }

    /**
     * 处理其他未知异常。
     */
    @ExceptionHandler(Exception.class)
    public ResponseResult handleUnknownException(Exception exception) {
        exception.printStackTrace();

        return ResponseResult.error("服务器开小差了,请稍后重试");
    }

}
