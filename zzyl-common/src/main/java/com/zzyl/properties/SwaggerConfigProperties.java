package com.zzyl.properties;

import lombok.*;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.io.Serializable;

/**
 *  Swagger配置类
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "zzyl.swagger")
public class SwaggerConfigProperties implements Serializable {

    public String swaggerPath;

    public String title;

    public String description;

    public String contactName;

    public String contactUrl;

    public String contactEmail;
}
