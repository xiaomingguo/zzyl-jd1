package com.zzyl.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * JWT配置
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "zzyl.jwt")
public class JwtProperties {
    private String base64EncodedSecretKey;
    private int ttl;
}
