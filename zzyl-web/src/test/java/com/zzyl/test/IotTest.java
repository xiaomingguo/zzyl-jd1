package com.zzyl.test;

import cn.hutool.json.JSONUtil;
import com.aliyun.iot20180120.Client;
import com.aliyun.iot20180120.models.QueryProductListRequest;
import com.aliyun.iot20180120.models.QueryProductListResponse;
import com.zzyl.properties.AliIoTConfigProperties;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class IotTest {

    @Autowired
    private Client client;

    @Autowired
    private AliIoTConfigProperties aliIoTConfigProperties;


    @Test
    public void testQueryProductList() throws Exception {
        //封装查询参数
        QueryProductListRequest request = new QueryProductListRequest();
        request.setCurrentPage(1);//第几页
        request.setPageSize(10);//每页条数
        request.setIotInstanceId(aliIoTConfigProperties.getIotInstanceId());//实例id


        QueryProductListResponse response = client.queryProductList(request);
        System.out.println(JSONUtil.toJsonStr(response.getBody().getData().getList().getProductInfo()));
    }


}
