package com.zzyl.controller.app;

import cn.hutool.core.date.LocalDateTimeUtil;
import com.zzyl.base.PageResponse;
import com.zzyl.base.ResponseResult;
import com.zzyl.controller.BaseController;
import com.zzyl.entity.Order;
import com.zzyl.service.OrderService;
import com.zzyl.utils.ObjectUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class AppOrderController extends BaseController {

    //下单检查--无需实现
    @PostMapping("/customer/orders/check")
    public ResponseResult createOrderCheck() {
        return success(null);
    }

    @Autowired
    private OrderService orderService;

    //下单
    @PostMapping("/customer/orders")
    public ResponseResult createOrder(@RequestBody Order order) {
        orderService.createOrder(order);
        //1. 申请微信支付码


        return success(order);
    }

    //订单查询
    @GetMapping("/customer/orders/order/page")
    public ResponseResult searchOrders(
            Integer status, String orderNo, String elderlyName, String creator,
            Long startTime, Long endTime, Integer pageNum, Integer pageSize) {
        PageResponse page = orderService.findByPage(status, orderNo, elderlyName, creator, ObjectUtil.isEmpty(startTime) ? null : LocalDateTimeUtil.of(startTime), ObjectUtil.isEmpty(endTime) ? null : LocalDateTimeUtil.of(endTime), pageNum, pageSize);
        return ResponseResult.success(page);
    }
}
