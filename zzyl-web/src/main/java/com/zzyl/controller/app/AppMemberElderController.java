package com.zzyl.controller.app;

import com.zzyl.base.PageResponse;
import com.zzyl.base.ResponseResult;
import com.zzyl.controller.BaseController;
import com.zzyl.dto.MemberElderDto;
import com.zzyl.entity.MemberElder;
import com.zzyl.service.MemberElderService;
import com.zzyl.utils.UserThreadLocal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AppMemberElderController extends BaseController {

    @Autowired
    private MemberElderService memberElderService;

    //绑定老人
    @PostMapping("/customer/memberElder/add")
    public ResponseResult save(@RequestBody MemberElderDto memberElderDto) {
        memberElderService.saveMemberElder(memberElderDto);
        return success();
    }


    //分页查询客户老人关联记录
    @GetMapping("/customer/memberElder/list-by-page")
    public ResponseResult findByPage(Long memberId, Long elderId, Integer pageNum, Integer pageSize) {
        if (memberId == null){
            memberId = UserThreadLocal.get();
        }
        PageResponse<MemberElder> page = memberElderService.findByPage(memberId, elderId, pageNum, pageSize);
        return success(page);
    }


    //我的家人列表(不分页)
    @GetMapping("/customer/memberElder/my")
    public ResponseResult my() {
        Long memberId = UserThreadLocal.get();
        PageResponse<MemberElder> page = memberElderService.findByPage(memberId, null, 1, 1000);
        return success(page.getRecords());
    }
}
