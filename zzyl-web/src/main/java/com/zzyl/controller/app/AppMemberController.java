package com.zzyl.controller.app;

import com.aliyun.iot20180120.Client;
import com.aliyun.iot20180120.models.QueryDevicePropertyStatusRequest;
import com.aliyun.iot20180120.models.QueryDevicePropertyStatusResponse;
import com.zzyl.base.ResponseResult;
import com.zzyl.controller.BaseController;
import com.zzyl.dto.CustomerUserLoginDto;
import com.zzyl.properties.AliIoTConfigProperties;
import com.zzyl.service.DeviceDataService;
import com.zzyl.service.MemberService;
import com.zzyl.vo.DeviceDataGraphVo;
import com.zzyl.vo.LoginVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AppMemberController extends BaseController {

    @Autowired
    MemberService memberService;

    //微信登录
    @PostMapping("/customer/user/login")
    public ResponseResult login(@RequestBody CustomerUserLoginDto dto){
        LoginVo loginVo = memberService.login(dto);
        return success(loginVo);
    }

    @Autowired
    private Client client;

    @Autowired
    private AliIoTConfigProperties aliIoTConfigProperties;

    //最新设备的状态
    @PostMapping("/customer/user/QueryDevicePropertyStatus")
    public ResponseResult queryDevicePropertyStatus(@RequestBody QueryDevicePropertyStatusRequest request)throws Exception {
        request.setIotInstanceId(aliIoTConfigProperties.getIotInstanceId());
        QueryDevicePropertyStatusResponse response = client.queryDevicePropertyStatus(request);
        return success(response.getBody().getData());
    }

    @Autowired
    private DeviceDataService deviceDataService;

    //按日查询设备数据
    @GetMapping("/customer/user/queryDeviceDataListByDay")
    public ResponseResult queryDeviceDataListByDay(String iotId, String functionId, Long startTime, Long endTime) {
        List<DeviceDataGraphVo> list = deviceDataService.queryDeviceDataListByDay(iotId, functionId, startTime, endTime);
        return ResponseResult.success(list);
    }
}
