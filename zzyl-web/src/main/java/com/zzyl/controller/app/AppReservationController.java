package com.zzyl.controller.app;

import com.zzyl.base.PageResponse;
import com.zzyl.base.ResponseResult;
import com.zzyl.controller.BaseController;
import com.zzyl.entity.Reservation;
import com.zzyl.service.ReservationService;
import com.zzyl.vo.TimeCountVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AppReservationController extends BaseController {

    @Autowired
    private ReservationService reservationService;

    //查询取消预约数量
    @GetMapping("/customer/reservation/cancelled-count")
    public ResponseResult getCancelCount() {
        int count = reservationService.getCancelCount();
        return success(count);
    }

    //查询剩余预约次数
    @GetMapping("/customer/reservation/countByTime")
    public ResponseResult getRemainingCount(Long time) {
        List<TimeCountVo> list = reservationService.getRemainingCount(time);
        return success(list);
    }

    //新增预约
    @PostMapping("/customer/reservation")
    public ResponseResult save(@RequestBody Reservation reservation){
        reservationService.saveReservation(reservation);
        return success();
    }

    //分页查询我的预约
    @GetMapping("/customer/reservation/page")
    public ResponseResult findByPage(Integer status,
                                         @RequestParam(defaultValue = "1") Integer pageNum,
                                         @RequestParam(defaultValue = "10")Integer pageSize){

        PageResponse<Reservation>  page = reservationService.findByPage(status,pageNum,pageSize);
        return success(page);
    }

    //取消预约
    @PutMapping("/customer/reservation/{id}/cancel")
    public ResponseResult cancel(@PathVariable("id") Long id){
        //准备一个对象
        Reservation reservation = new Reservation();
        reservation.setId(id);
        reservation.setStatus(2);//取消

        //修改
        reservationService.updateById(reservation);
        return success();
    }


}
