package com.zzyl.controller.app;

import com.zzyl.base.PageResponse;
import com.zzyl.base.ResponseResult;
import com.zzyl.controller.BaseController;
import com.zzyl.entity.NursingProject;
import com.zzyl.service.NursingProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

//护理项目服务
@RestController
public class AppNursingProjectController extends BaseController {

    @Autowired
    private NursingProjectService nursingProjectService;

    //分页查询
    @GetMapping("/customer/orders/project/page")
    public ResponseResult findByPage(
            String name, Integer status,
            @RequestParam(defaultValue = "1") Integer pageNum,
            @RequestParam(defaultValue = "10") Integer pageSize){

        PageResponse<NursingProject> page = nursingProjectService.findByPage(name, status, pageNum, pageSize);
        return success(page);
    }

    //主键查询
    @GetMapping("/customer/orders/project/{id}")
    public ResponseResult findById(@PathVariable("id") Long id){
        NursingProject nursingProject = nursingProjectService.getById(id);
        return success(nursingProject);
    }
}
