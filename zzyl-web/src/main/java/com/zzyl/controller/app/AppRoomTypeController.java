package com.zzyl.controller.app;

import com.zzyl.base.ResponseResult;
import com.zzyl.controller.BaseController;
import com.zzyl.entity.RoomType;
import com.zzyl.service.RoomTypeService;
import com.zzyl.vo.RoomTypeVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 房型管理
 */
@RestController
public class AppRoomTypeController extends BaseController {
    @Autowired
    private RoomTypeService roomTypeService;

    //根据状态查询房型
    @GetMapping("/customer/roomTypes")
    public ResponseResult findRoomTypeListByStatus(@RequestParam("status") Integer status) {
        RoomType roomType = new RoomType();
        roomType.setStatus(status);
        List<RoomTypeVo> list = roomTypeService.findList(roomType);
        return success(list);
    }
}
