package com.zzyl.controller.web;

import com.aliyun.iot20180120.Client;
import com.aliyun.iot20180120.models.*;
import com.zzyl.base.PageResponse;
import com.zzyl.base.ResponseResult;
import com.zzyl.controller.BaseController;
import com.zzyl.dto.DeviceDto;
import com.zzyl.dto.DevicePageQueryDto;
import com.zzyl.entity.Device;
import com.zzyl.properties.AliIoTConfigProperties;
import com.zzyl.service.DeviceService;
import com.zzyl.vo.DeviceVo;
import com.zzyl.vo.ProductVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DeviceController extends BaseController {

    @Autowired
    private DeviceService deviceService;

    //从物联网平台同步产品列表
    @PostMapping("/iot/syncProductList")
    public ResponseResult syncProductList() {
        deviceService.syncProductList();
        return success();
    }

    //查询所有产品列表
    @GetMapping("/iot/allProduct")
    public ResponseResult findAllProduct() {
        List<ProductVo> list = deviceService.findAllProduct();
        return success(list);
    }

    //新增设备
    @PostMapping("/iot/RegisterDevice")
    public ResponseResult registerDevice(@RequestBody DeviceDto deviceDto) {
        deviceService.registerDevice(deviceDto);
        return success();
    }

    //分页查询设备列表
    @GetMapping("/iot/pageQueryDevice")
    public ResponseResult pageQueryDevice(DevicePageQueryDto devicePageQueryDto) {
        PageResponse<Device> pageResponse = deviceService.findByPage(devicePageQueryDto);
        return success(pageResponse);
    }

    //查询设备详细数据
    @PostMapping("/iot/QueryDeviceDetail")
    public ResponseResult queryDeviceDetail(@RequestBody QueryDeviceDetailRequest request) {
        DeviceVo deviceVo = deviceService.queryDeviceDetail(request);
        return success(deviceVo);
    }

    @Autowired
    private Client client;

    @Autowired
    private AliIoTConfigProperties aliIoTConfigProperties;

    //查询设备最新数据
    @PostMapping("/iot/QueryThingModelPublished")
    public ResponseResult queryThingModelPublished(@RequestBody QueryThingModelPublishedRequest request) throws Exception {
        request.setIotInstanceId(aliIoTConfigProperties.getIotInstanceId());
        QueryThingModelPublishedResponse response = client.queryThingModelPublished(request);
        return success(response.getBody().getData());
    }

    //最新设备的状态
    @PostMapping("/iot/QueryDevicePropertyStatus")
    public ResponseResult queryDevicePropertyStatus(@RequestBody QueryDevicePropertyStatusRequest request)throws Exception {
        request.setIotInstanceId(aliIoTConfigProperties.getIotInstanceId());
        QueryDevicePropertyStatusResponse response = client.queryDevicePropertyStatus(request);
        return success(response.getBody().getData());
    }


}