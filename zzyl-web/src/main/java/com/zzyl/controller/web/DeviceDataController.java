package com.zzyl.controller.web;

import com.zzyl.base.PageResponse;
import com.zzyl.base.ResponseResult;
import com.zzyl.controller.BaseController;
import com.zzyl.dto.DeviceDataPageReqDto;
import com.zzyl.entity.DeviceData;
import com.zzyl.service.DeviceDataService;
import com.zzyl.vo.DeviceDataVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//设备数据
@RestController
public class DeviceDataController extends BaseController {

    @Autowired
    private DeviceDataService deviceDataService;

    //获取设备数据分页结果
    @GetMapping("/device-data/get-page")
    public ResponseResult getDeviceDataPage(DeviceDataPageReqDto deviceDataPageReqDto){
        PageResponse<DeviceData> pageResponse = deviceDataService.findByPage(deviceDataPageReqDto);
        return success(pageResponse);
    }
}