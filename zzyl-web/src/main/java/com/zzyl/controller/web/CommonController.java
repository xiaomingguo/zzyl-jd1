package com.zzyl.controller.web;

import com.zzyl.base.ResponseResult;
import com.zzyl.utils.OSSTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

//通用接口
@RestController
public class CommonController {

    @Autowired
    private OSSTemplate ossTemplate;

    /**
     * 文件上传
     */
    @PostMapping("/common/upload")
    public ResponseResult upload( MultipartFile file) throws Exception {

        // 校验是否为图片文件
        if (file.getSize() == 0) {
            throw new RuntimeException("上传图片不能为空");
        }

        // 获得原始文件名
        String originalFilename = file.getOriginalFilename();

        // 获得文件扩展名
        String extension = originalFilename.substring(originalFilename.lastIndexOf("."));
        String fileName = UUID.randomUUID().toString() + extension;

        String filePath = ossTemplate.store(fileName, file.getInputStream());
        return ResponseResult.success(filePath);
    }
}
