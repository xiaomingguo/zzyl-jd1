package com.zzyl.controller.web;


import com.zzyl.base.PageResponse;
import com.zzyl.base.ResponseResult;
import com.zzyl.controller.BaseController;
import com.zzyl.entity.NursingProject;
import com.zzyl.service.NursingProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 护理项目表 前端控制器
 * </p>
 *
 * @author gxm
 * @since 2024-08-15
 */
@RestController
public class NursingProjectController extends BaseController {

    @Autowired
    private NursingProjectService nursingProjectService;

    //分页条件查询
    @GetMapping("/nursing_project")
    public ResponseResult findByPage(String name, Integer status,
                                     @RequestParam(defaultValue = "1") Integer pageNum,
                                     @RequestParam(defaultValue = "10") Integer pageSize) {
        PageResponse<NursingProject>  pageResponse = nursingProjectService.findByPage(name,status,pageNum,pageSize);
        return  success(pageResponse);
    }

    //新增
    @PostMapping("/nursing_project")
    public ResponseResult save(@RequestBody NursingProject nursingProject){
        nursingProjectService.save(nursingProject);
        return success();
    }

    //主键查询
    @GetMapping("/nursing_project/{id}")
    public ResponseResult findById(@PathVariable("id") Long id){
        NursingProject nursingProject = nursingProjectService.getById(id);
        return success(nursingProject);
    }

    //更新护理项目信息
    @PutMapping("/nursing_project/{id}")
    public ResponseResult update(@RequestBody NursingProject nursingProject) {
        nursingProjectService.updateById(nursingProject);
        return success();
    }

    //启用禁用
    @PutMapping("/nursing_project/{id}/status/{status}")
    public ResponseResult enableOrDisable(@PathVariable("id") Long id,@PathVariable("status") Integer status){
        //update np set status = #{status} where id = #{id}
        NursingProject nursingProject = new NursingProject();
        nursingProject.setId(id);
        nursingProject.setStatus(status);

        //mp的更新方法的动态sql特性
        nursingProjectService.updateById(nursingProject);
        return success();
    }

    //删除
    @DeleteMapping("/nursing_project/{id}")
    public ResponseResult deleteById(@PathVariable("id") Long id) {
        //todo 作业: 如果护理项目被引用,无法删除


        nursingProjectService.removeById(id);
        return success();
    }

    //查询所有护理项目
    @GetMapping("/nursing_project/all")
    public ResponseResult findAll(){
        //status = 1
        List<NursingProject> list = nursingProjectService.list();
        return success(list);
    }

}
