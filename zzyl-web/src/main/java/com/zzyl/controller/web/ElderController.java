
package com.zzyl.controller.web;

import com.zzyl.base.PageResponse;
import com.zzyl.base.ResponseResult;
import com.zzyl.entity.Elder;
import com.zzyl.service.ElderService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 客户老人关联 Controller
 */
@RestController
@RequestMapping("/elder")
public class ElderController {

    @Autowired
    private ElderService elderService;

    //老人分页查询
    @GetMapping("/pageQuery")
    public ResponseResult pageQuery(@RequestParam(value = "name", required = false) String name,
                                    @RequestParam(value = "idCardNo", required = false) String idCardNo,
                                    @RequestParam(value = "status", required = false) String status,
                                    @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                    @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        //todo 前端bug
        if (StringUtils.equals(status, "0")) {
            status = null;
        }

        PageResponse<Elder> page = elderService.findByPage(name, idCardNo, status, pageNum, pageSize);
        return ResponseResult.success(page);
    }
}


