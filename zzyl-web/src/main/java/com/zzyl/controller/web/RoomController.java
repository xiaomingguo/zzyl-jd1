package com.zzyl.controller.web;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zzyl.base.ResponseResult;
import com.zzyl.controller.BaseController;
import com.zzyl.entity.Bed;
import com.zzyl.entity.Room;
import com.zzyl.exception.BusinessException;
import com.zzyl.service.BedService;
import com.zzyl.service.RoomService;
import com.zzyl.vo.RoomVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RoomController extends BaseController {

    @Autowired
    private RoomService roomService;

    @Autowired
    private BedService bedService;

    //新增房间
    @PostMapping("/room/add")
    public ResponseResult save(@RequestBody Room room){
        roomService.saveOrUpdateRoom(room);
        return success();
    }

    //查询指定楼层下的房间
    @GetMapping("/room/getRoomsByFloorId/{floorId}")
    public ResponseResult findByFloorId(@PathVariable("floorId") Long floorId){
        List<RoomVo> roomVoList = roomService.findByFloorId(floorId);
        return success(roomVoList);
    }

    //查询房间详情
    @GetMapping("/room/get/{id}")
    public ResponseResult findById(@PathVariable("id") Long id){
        RoomVo roomVo = roomService.findRoomById(id);
        return success(roomVo);
    }

    //修改房间
    @PutMapping("/room/update")
    public ResponseResult update(@RequestBody Room room){
        roomService.saveOrUpdateRoom(room);
        return success();
    }


    //删除房间
    @DeleteMapping("/room/delete/{id}")
    public ResponseResult deleteById(@PathVariable("id") Long id){
        //查询一下当前房间下是否有床位, 如果有不能删除
        //select count(*) from base_bed where room_id = 房间id
        LambdaQueryWrapper<Bed> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Bed::getRoomId,id);
        long count = bedService.count(wrapper);
        if (count > 0){
            throw  new BusinessException("当前房间内有床位,无法删除");
        }

        //删除
        roomService.removeById(id);
        return success();
    }

    //获取所有房间（智能床位)
    @GetMapping("/room/getRoomsWithDeviceByFloorId/{floorId}")
    public ResponseResult getRoomsWithDeviceByFloorId(@PathVariable(name = "floorId") Long floorId){
        List<RoomVo> list  = roomService.getRoomsWithDeviceByFloorId(floorId);
        return success(list);
    }
}
