package com.zzyl.controller.web;


import com.zzyl.base.PageResponse;
import com.zzyl.base.ResponseResult;
import com.zzyl.controller.BaseController;
import com.zzyl.dto.NursingPlanDto;
import com.zzyl.entity.NursingPlan;
import com.zzyl.entity.NursingProject;
import com.zzyl.service.NursingPlanService;
import com.zzyl.vo.NursingPlanVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 护理计划表 前端控制器
 * </p>
 *
 * @author gxm
 * @since 2024-08-15
 */
@RestController
public class NursingPlanController extends BaseController {

    @Autowired
    private NursingPlanService nursingPlanService;

    //分页条件查询
    @GetMapping("/nursing/plan/search")
    public ResponseResult findByPage(String name, Integer status,
                                     @RequestParam(defaultValue = "1") Integer pageNum,
                                     @RequestParam(defaultValue = "10") Integer pageSize) {
        PageResponse<NursingPlan> pageResponse = nursingPlanService.findByPage(name,status,pageNum,pageSize);
        return  success(pageResponse);
    }

    //新增
    @PostMapping("/nursing/plan")
    public ResponseResult save(@RequestBody NursingPlanDto nursingPlanDto){
        nursingPlanService.saveNursingPlan(nursingPlanDto);
        return success();
    }

    //主键查询
    @GetMapping("/nursing/plan/{id}")
    public ResponseResult findById(@PathVariable("id") Long id){
        NursingPlanVo vo = nursingPlanService.findById(id);
        return success(vo);
    }

    //修改
    @PutMapping("/nursing/plan/{id}")
    public ResponseResult update(@RequestBody NursingPlanDto nursingPlanDto){
        nursingPlanService.updateNursingPlan(nursingPlanDto);
        return success();
    }

    //删除
    @DeleteMapping("/nursing/plan/{id}")
    public ResponseResult deleteById(@PathVariable("id") Long id){
        nursingPlanService.deleteById(id);
        return success();
    }

    //启用/禁用护理计划
    @PutMapping("/nursing/{id}/status/{status}")
    public ResponseResult enableOrDisable(@PathVariable Long id, @PathVariable Integer status) {
        NursingPlan nursingPlan = new NursingPlan();
        nursingPlan.setId(id);
        nursingPlan.setStatus(status);
        nursingPlanService.updateById(nursingPlan);

        return ResponseResult.success();
    }

    //查询所有护理计划
    @GetMapping("/nursing/plan")
    public ResponseResult getAllNursingPlan() {
        List<NursingPlan> list = nursingPlanService.list();
        return ResponseResult.success(list);
    }


}
