package com.zzyl.controller.web;


import com.zzyl.controller.BaseController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 护理计划和项目关联表 前端控制器
 * </p>
 *
 * @author gxm
 * @since 2024-08-15
 */
@RestController
public class NursingProjectPlanController extends BaseController {

}
