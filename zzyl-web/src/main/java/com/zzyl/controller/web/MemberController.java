package com.zzyl.controller.web;

import com.zzyl.base.PageResponse;
import com.zzyl.base.ResponseResult;
import com.zzyl.controller.BaseController;
import com.zzyl.service.MemberService;
import com.zzyl.vo.MemberVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

//小程端用户
@RestController
public class MemberController extends BaseController {

    @Autowired
    private MemberService memberService;

    //分页查询
    @GetMapping("/c-user/page")
    public ResponseResult findByPage(
            String nickname,String phone,
            @RequestParam(defaultValue = "1") Integer pageNum,
            @RequestParam(defaultValue = "10") Integer pageSize){
        PageResponse<MemberVo> pageResponse =  memberService.findByPage(nickname,phone,pageNum,pageSize);
        return success(pageResponse);
    }
}
