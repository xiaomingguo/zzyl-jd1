package com.zzyl.controller.web;

import com.zzyl.base.ResponseResult;
import com.zzyl.controller.BaseController;
import com.zzyl.entity.RoomType;
import com.zzyl.service.RoomTypeService;
import com.zzyl.vo.RoomTypeVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RoomTypeController extends BaseController {

    @Autowired
    private RoomTypeService roomTypeService;

    //保存
    @PostMapping("/roomTypes")
    public ResponseResult save(@RequestBody RoomType roomType) {
        roomTypeService.save(roomType);
        return success();
    }

    //主键删除
    @DeleteMapping("/roomTypes/{id}")
    public ResponseResult deleteById(@PathVariable Long id) {
        roomTypeService.removeById(id);
        return success();
    }

    //修改
    @PutMapping("/roomTypes/{id}")
    public ResponseResult modifyRoomType(@PathVariable Long id, @RequestBody RoomType roomType) {
        roomType.setId(id);
        roomTypeService.saveOrUpdate(roomType);
        return success();
    }

    //主键查询
    @GetMapping("/roomTypes/{id}")
    public ResponseResult findById(@PathVariable Long id) {
        RoomType roomType = roomTypeService.getById(id);
        return success(roomType);
    }

    //查询房型列表
    @GetMapping("/roomTypes")
    public ResponseResult findList() {
        RoomType roomType = new RoomType();
        List<RoomTypeVo> list = roomTypeService.findList(roomType);
        return success(list);
    }

    //查询指定状态的房型列表
    @GetMapping("/roomTypes/status/{status}")
    public ResponseResult findListByStatus(@PathVariable Integer status) {
        RoomType roomType = new RoomType();
        roomType.setStatus(status);

        List<RoomTypeVo> list = roomTypeService.findList(roomType);
        return success(list);
    }

    //查询指定名称的房型列表
    @GetMapping("/roomTypes/typeName/{name}")
    public ResponseResult findListByName(@PathVariable String name) {
        RoomType roomType = new RoomType();
        roomType.setName(name);

        List<RoomTypeVo> list = roomTypeService.findList(roomType);
        return success(list);
    }

    //启用禁用
    @PutMapping("/roomTypes/{id}/status/{status}")
    public ResponseResult enableOrDisable(@PathVariable Long id, @PathVariable Integer status) {
        RoomType roomType = new RoomType();
        roomType.setId(id);
        roomType.setStatus(status);
        roomTypeService.saveOrUpdate(roomType);
        return success();
    }
}
