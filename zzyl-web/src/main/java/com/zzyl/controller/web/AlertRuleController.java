package com.zzyl.controller.web;

import com.zzyl.base.PageResponse;
import com.zzyl.base.ResponseResult;
import com.zzyl.controller.BaseController;
import com.zzyl.entity.AlertRule;
import com.zzyl.service.AlertRuleService;
import com.zzyl.vo.AlertRuleVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AlertRuleController extends BaseController {

    @Autowired
    private AlertRuleService alertRuleService;

    // 创建告警规则
    @PostMapping("/alert-rule/create")
    public ResponseResult createAlertRule(@RequestBody AlertRule alertRule) {
        alertRuleService.save(alertRule);
        return success();
    }

    //查询报警规则
    @GetMapping("/alert-rule/get-page")
    public ResponseResult getAlertRulePage(Integer pageNum, Integer pageSize, String alertRuleName, String productKey, String functionName) {
        PageResponse<AlertRuleVo> pageResponse = alertRuleService.getAlertRulePage(pageNum, pageSize, alertRuleName, productKey, functionName);
        return success(pageResponse);
    }
}