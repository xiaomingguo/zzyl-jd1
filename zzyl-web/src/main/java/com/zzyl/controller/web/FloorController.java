package com.zzyl.controller.web;

import com.zzyl.base.ResponseResult;
import com.zzyl.controller.BaseController;
import com.zzyl.dto.FloorDto;
import com.zzyl.entity.Floor;
import com.zzyl.service.FloorService;
import com.zzyl.vo.FloorVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//楼层管理
@RestController
public class FloorController extends BaseController {

    @Autowired
    private FloorService floorService;

    //查询所有楼层
    @GetMapping("/floor/getAll")
    public ResponseResult findAll(){
        List<Floor> list = floorService.list();
        return success(list);
    }

    //新增楼层
    @PostMapping("/floor/add")
    public ResponseResult save(@RequestBody Floor floor){
        floorService.save(floor);
        return success();
    }

    //根据id查询
    @GetMapping("/floor/get/{id}")
    public ResponseResult findById(@PathVariable("id") Long id){
        Floor floor = floorService.getById(id);
        return success(floor);
    }


    //修改楼层
    @PutMapping("/floor/update")
    public ResponseResult update(@RequestBody Floor floor){
        floorService.updateById(floor);
        return success();
    }

    //根据id删除
    @DeleteMapping("/floor/delete/{id}")
    public ResponseResult deleteById(@PathVariable("id") Long id){
        floorService.removeById(id);
        return success();
    }


    //获取所有楼层 （智能楼层）
    @GetMapping("/floor/getAllFloorsWithDevice")
    public ResponseResult getAllFloorsWithDevice(){
        List<Floor> list = floorService.getAllFloorsWithDevice();
        return success(list);
    }








    //=============================监控阶段使用===============================//
    @GetMapping("/floor/getAllWithRoomAndBed")
    @ApiOperation(value = "获取所有楼层 （包含房间和床位）", notes = "无需参数，获取所有楼层，返回楼层信息列表")
    public ResponseResult getAllWithRoomAndBed() {
        List<FloorVo> list = floorService.getAllWithRoomAndBed();
        return success(list);
    }

}