package com.zzyl.controller.web;

import com.zzyl.base.PageResponse;
import com.zzyl.base.ResponseResult;
import com.zzyl.entity.NursingLevel;
import com.zzyl.service.NursingLevelService;
import com.zzyl.vo.NursingLevelVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

//护理等级管理
@RestController
public class NursingLevelController {

    @Autowired
    private NursingLevelService nursingLevelService;

    //分页查询
    @GetMapping("/nursingLevel/listByPage")
    public ResponseResult findByPage(
            @RequestParam(defaultValue = "1") Integer pageNum,
            @RequestParam(defaultValue = "10") Integer pageSize,
            String name, Integer status) {
        PageResponse<NursingLevelVo> pageResponse = nursingLevelService.findByPage(pageNum, pageSize, name, status);
        return ResponseResult.success(pageResponse);
    }

    //新增
    @PostMapping("/nursingLevel/insert")
    public ResponseResult save(@RequestBody NursingLevel nursingLevel) {
        nursingLevelService.save(nursingLevel);
        return ResponseResult.success();
    }

    //根据id查询
    @GetMapping("/nursingLevel/{id}")
    public ResponseResult findById(@PathVariable("id") Long id) {
        NursingLevel nursingLevel = nursingLevelService.getById(id);
        return ResponseResult.success(nursingLevel);
    }

    //更新
    @PutMapping("/nursingLevel/update")
    public ResponseResult update(@RequestBody NursingLevel nursingLevel) {
        nursingLevelService.saveOrUpdate(nursingLevel);
        return ResponseResult.success();
    }

    //主键删除
    @DeleteMapping("/nursingLevel/delete/{id}")
    public ResponseResult deleteById(@PathVariable("id") Long id) {
        nursingLevelService.removeById(id);
        return ResponseResult.success();
    }

    //启用/禁用
    @PutMapping("/nursingLevel/{id}/status/{status}")
    public ResponseResult enableOrDisable(@PathVariable Long id, @PathVariable Integer status) {
        NursingLevel nursingLevel = new NursingLevel();
        nursingLevel.setId(id);
        nursingLevel.setStatus(status);
        nursingLevelService.saveOrUpdate(nursingLevel);
        return ResponseResult.success();
    }


    //    //查询所有
//    @GetMapping("/nursingLevel/listAll")
//    public ResponseResult findAll() {
//        List<NursingLevelVo> nursingLevelDtos = nursingLevelService.findAll();
//        return ResponseResult.success(nursingLevelDtos);
//    }
//
//    @PostMapping("/nursingLevel/insertBatch")
//    @ApiOperation(value = "批量插入护理等级信息")
//    public ResponseResult insertBatch(
//            @ApiParam(value = "护理等级数据列表", required = true)
//            @RequestBody List<NursingLevelDto> nursingLevelDtos) {
//        nursingLevelService.insertBatch(nursingLevelDtos);
//        return ResponseResult.success();
//    }
//
}
